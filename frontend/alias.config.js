const path = require('path');

// alias 路径别名配置
const alias = {
    '@': path.resolve(__dirname, './src'),
    '@i18n': path.resolve(__dirname, './i18n'),
    '@libs': path.resolve(__dirname, './libs'),
    '@assets': path.resolve(__dirname, './src/assets'),
    '@components': path.resolve(__dirname, './src/components'),
};

// 这么导出是为了让 WebStorm 识别出
module.exports = {
    resolve: {
        alias: alias
    }
}

import nodeResolve from 'rollup-plugin-node-resolve';
import Vue from '@vitejs/plugin-vue';

export default {
    input: {
        main: './index.html',
    },
    output: {
        format: 'esm',
        chunkFileNames: 'assets/js/[name]-[hash].js',
        entryFileNames: 'assets/js/[name]-[hash].js',
        assetFileNames: 'assets/[ext]/[name]-[hash].[ext]',
        manualChunks(id) {
            // 将导入的静态资源分拆打包
            if (id.includes('node_modules')) {
                return id.toString().split('node_modules/')[1].split('/')[0].toString();
            }
        }
    },
    plugins: [
        nodeResolve(),
    ],
    globals: {
        // 在 UMD 构建模式下为这些外部化的依赖提供一个全局变量
        vue: Vue
    },
    // 忽略打包指定后缀的文件列表
    external: [],
}

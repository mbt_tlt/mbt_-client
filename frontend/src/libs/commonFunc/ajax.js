/**
 * getCookie - 读取指定 cookie 值
 *
 * @const
 * @export
 * @function
 * @param { string } cookieName - cookie 名
 * @return { string } 对应 cookie 值, 若不存在则返回空字符串
 * */
export const getCookie = function (cookieName) {
    const cookieList = document.cookie.split(';');

    for (let i = 0; i < cookieList.length; i++) {
        const arr = cookieList[i].split('=');
        if (cookieName === arr[0]) {
            return arr[1];
        }
    }

    return '';
}

/**
 * createXMLHttpRequest - 创建 AJAX XMLHttpRequest 通信对象
 *
 * @const
 * @export
 * @function
 * @return { XMLHttpRequest|any }
 * */
export const createXMLHttpRequest = function () {
    if (window.XMLHttpRequest) {
        return new XMLHttpRequest();
    } else if (window.ActiveXObject) {
        return new ActiveXObject("Microsoft.XMLHTTP");
    }
}

/**
 * convertDataAJAX - AJAX 数据 JSON 化
 *
 * @const
 * @export
 * @function
 * @param { Object|any } data - 需要转换的数据
 * @return { String|any }
 * */
export const convertDataAJAX = function (data) {
    if (typeof data === 'object') {
        return `data=${ JSON.stringify(data) }`;
    } else {
        return `data=${ data }`;
    }
}

/**
 * anomalyCaptureAJAX - 自定义封装 ajax 通信方法
 *
 * @const
 * @export
 * @function
 * @param { Object } configObj - ajax 配置列表
 * */
export const anomalyCaptureAJAX = function (configObj) {
    /**
     * ajaxData - ajax 配置参数参数
     *
     * @type Object
     * */
    const ajaxData = {
        type: String(configObj.type).toUpperCase() || "GET",
        url: configObj.url || "",
        async: configObj.async || "true",
        data: configObj.data || null,
        dataType: configObj.dataType || "text",
        header: configObj.header || {
            "Content-Type": "application/x-www-form-urlencoded",
            "x-csrf-token": getCookie('csrfToken'),
        },
        beforeSend: configObj.beforeSend || function () {},
        afterSend: configObj.beforeSend || function () {},
        success: configObj.success || function () {},
        error: configObj.error || function () {}
    }

    // 执行通信前操作
    ajaxData.beforeSend();

    const xhr = createXMLHttpRequest();
    xhr.responseType = ajaxData.dataType;
    xhr.open(ajaxData.type, ajaxData.url, ajaxData.async);
    Object.keys(ajaxData.header).forEach((headerKey) => {
        xhr.setRequestHeader(headerKey, ajaxData.header[headerKey]);
    });
    xhr.send(convertDataAJAX(ajaxData.data));
    xhr.onreadystatechange = function () {
        if (Number(xhr.readyState) === 4) {
            if (Number(xhr.status) === 200) {
                ajaxData.success(xhr.response);
            } else {
                ajaxData.error();
            }
        }
    }

    // 执行通信后操作
    ajaxData.afterSend();
}

export default {
    getCookie,
    createXMLHttpRequest,
    convertDataAJAX,
    anomalyCaptureAJAX,
}

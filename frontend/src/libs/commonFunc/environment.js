/**
 * getSystem - 获取当前系统
 *
 * @const
 * @function
 * @param { string } config - 配置字符串
 * @return { string } 系统类型
 * */
const getSystem = function (config) {
    config = config.toLowerCase();

    if (config.indexOf('windows') !== -1) {
        return 'windows';
    } else if (config.indexOf('linux') !== -1) {
        return 'linux';
    } else if (config.indexOf('mac') !== -1) {
        return 'mac';
    } else if (config.indexOf('iphone') !== -1) {
        return 'ios';
    } else if (config.indexOf('android') !== -1) {
        return 'android';
    } else if (config.indexOf('harmony') !== -1) {
        return 'harmony';
    } else {
        return 'unknown';
    }
}

/**
 * getTerminal - 获取当前运行终端
 *
 * @const
 * @function
 * @param { string } config - 配置字符串
 * @return { string } 运行终端类型
 * */
const getTerminal = function (config) {
    config = config.toLowerCase();

    if (config.indexOf('iphone') !== -1) {
        return 'mobile';
    } else if (config.indexOf('android') !== -1) {
        return 'mobile';
    } else if (config.indexOf('harmony') !== -1) {
        return 'mobile';
    } else if (config.indexOf('mobi') !== -1) {
        return 'mobile';
    } else if (config.indexOf('ipad') !== -1) {
        return 'ipad';
    } else if (config.indexOf('safari') !== -1 && config.indexOf('electron') === -1) {
        return 'browser';
    } else if (config.indexOf('electron') !== -1) {
        return 'electron';
    } else if (config.indexOf('micromessenger') !== -1) {
        return 'weichat';
    } else {
        return 'unknown';
    }
}

/**
 * getBrowserType - 获取当前浏览器版本
 *
 * @const
 * @function
 * @param { string } config - 配置字符串
 * @return { string } 运行浏览器类型
 * */
const getBrowserType = function (config) {
    config = config.toLowerCase();

    if (config.indexOf('msie') !== -1) {
        return 'ie';
    } else if (config.indexOf('firefox') !== -1) {
        return 'firefox';
    } else if (config.indexOf('chrome') !== -1) {
        return 'chrome';
    } else {
        return 'unknown';
    }
}

/**
 * getEnvironmentObj - 获取运行环境对象
 *
 * @export
 * @const
 * @function
 * @return { Object } 当前运行环境对象
 * */
export const getEnvironmentObj = function () {
    const environment = navigator.userAgent.toLowerCase();
    const reObj = {};

    // 判断当前系统
    reObj['system'] = getSystem(environment);

    // 判断当前运行终端
    reObj['terminal'] = getTerminal(environment);

    // 判断浏览器类型
    reObj['browserType'] = getBrowserType(environment);

    return reObj;
}

export default {
    getEnvironmentObj
}

import zh_CN from '@i18n/zh-CN.json';
import en_US from '@i18n/en-US.json';
import ja_JP from '@i18n/ja-JP.json';


/**
 * i18nObj - i18n 对象
 *
 * @const
 * @type Object
 * */
const i18nObj = {
    'zh-CN': zh_CN,
    'en-US': en_US,
    'ja-JP': ja_JP,
}

/**
 * getI18nObj - 获取 i18n 对象
 *
 * @export
 * @const
 * @function
 * @return { Object } i18nObj
 * */
export const getI18nObj = function () {
    return i18nObj;
}

/**
 * i18nGuard - i18n 守卫函数
 * 
 * @const
 * @function
 * @param { string } key - i18n key
 * @param { Object } [replaceObj = {}] - 正则替换对象
 * @param { string } [language = navigator.language] - 指定的语言文本库, 默认为当前环境语言
 * @throws { ReferenceError } - 1.当指定 “language” 的 i18n 文件不存在时抛出
 * @throws { ReferenceError } - 2.当指定 “language” 的 i18n 文件中不存在指定的 “key” 时抛出
 * */
const i18nGuard = function (
    key,
    replaceObj,
    language
) {
    // 检测指定的 i18n 文件是否存在
    const i18nObjSet = new Set(Object.keys(i18nObj));
    if (!i18nObjSet.has(language)) {
        throw ReferenceError(i18n('SyntaxError-i18nGuard-1', {
            language: language
        }, navigator.language));
    }

    // 检查指定的 i18n 文件是否包含指定的 key
    const i18nDataSet = new Set(Object.keys(i18nObj[language]));
    if (!i18nDataSet.has(key)) {
        throw ReferenceError(i18n('SyntaxError-i18nGuard-2', {
            language: language,
            key: key
        }, navigator.language));
    }
}

/**
 * regReplace - 文本正则替换
 *
 * @const
 * @function
 * @param { string } text - 需要替换的文本
 * @param { Object } replaceObj - 文本的替换对象
 * @return { string } 替换的文本
 * @throws { ReferenceError } - 当 “replaceObj” 缺少指定的替换关键字时 抛出
 * */
const regReplace = function (text, replaceObj) {
    const replaceReg = new RegExp(/\${(.+?)}/);

    let execResult = null;
    while (execResult = replaceReg.exec(text)) {
        const replaceKey = execResult[1].trim();

        if (replaceObj[replaceKey] !== void 0) {
            text = text.replace(replaceReg, replaceObj[replaceKey]);
        } else {
            throw ReferenceError(i18n('ReferenceError-regReplace', {
                replaceKey: replaceKey
            }, navigator.language));
        }
    }

    return text;
}

/**
 * i18n - i18n 国际化翻译
 *
 * @export
 * @const
 * @function
 * @param { string } key - i18n key
 * @param { Object } [replaceObj = {}] - 正则替换对象
 * @param { string } [language = navigator.language] - 指定的语言文本库, 默认为当前环境语言
 * */
export const i18n = function (
    key,
    replaceObj = {},
    language = navigator.language
) {
    try {
        i18nGuard(key, replaceObj, language);
    } catch (err) {
        console.error(err);
    }

    return regReplace(i18nObj[language][key], replaceObj);
}

/**
 * L10nSignSet - L10n 标识集
 *
 * @export
 * @const
 * @type Set
 * */
export const L10nSignSet = new Set([
    'zh-CN', // 简体中文(中国)
    'zh-TW', // 繁体中文(台湾地区)
    'zh-HK', // 繁体中文(香港)
    'en-HK', // 英语(香港)
    'en-US', // 英语(美国)
    'en-GB', // 英语(英国)
    'en-WW', // 英语(全球)
    'en-CA', // 英语(加拿大)
    'en-AU', // 英语(澳大利亚)
    'en-IE', // 英语(爱尔兰)
    'en-FI', // 英语(芬兰)
    'fi-FI', // 芬兰语(芬兰)
    'en-DK', // 英语(丹麦)
    'da-DK', // 丹麦语(丹麦)
    'en-IL', // 英语(以色列)
    'he-IL', // 希伯来语(以色列)
    'en-ZA', // 英语(南非)
    'en-IN', // 英语(印度)
    'en-NO', // 英语(挪威)
    'en-SG', // 英语(新加坡)
    'en-NZ', // 英语(新西兰)
    'en-ID', // 英语(印度尼西亚)
    'en-PH', // 英语(菲律宾)
    'en-TH', // 英语(泰国)
    'en-MY', // 英语(马来西亚)
    'en-XA', // 英语(阿拉伯)
    'ko-KR', // 韩文(韩国)
    'ja-JP', // 日语(日本)
    'nl-NL', // 荷兰语(荷兰)
    'nl-BE', // 荷兰语(比利时)
    'pt-PT', // 葡萄牙语(葡萄牙)
    'pt-BR', // 葡萄牙语(巴西)
    'fr-FR', // 法语(法国)
    'fr-LU', // 法语(卢森堡)
    'fr-CH', // 法语(瑞士)
    'fr-BE', // 法语(比利时)
    'fr-CA', // 法语(加拿大)
    'es-LA', // 西班牙语(拉丁美洲)
    'es-ES', // 西班牙语(西班牙)
    'es-AR', // 西班牙语(阿根廷)
    'es-US', // 西班牙语(美国)
    'es-MX', // 西班牙语(墨西哥)
    'es-CO', // 西班牙语(哥伦比亚)
    'es-PR', // 西班牙语(波多黎各)
    'de-DE', // 德语(德国)
    'de-AT', // 德语(奥地利)
    'de-CH', // 德语(瑞士)
    'ru-RU', // 俄语(俄罗斯)
    'it-IT', // 意大利语(意大利)
    'el-GR', // 希腊语(希腊)
    'no-NO', // 挪威语(挪威)
    'hu-HU', // 匈牙利语(匈牙利)
    'tr-TR', // 土耳其语(土耳其)
    'cs-CZ', // 捷克语(捷克共和国)
    'sl-SL', // 斯洛文尼亚语
    'pl-PL', // 波兰语(波兰)
    'sv-SE', // 瑞典语(瑞典)
    'es-CL', // 西班牙语 (智利)
]);

export default {
    getI18nObj,
    i18n,
    L10nSignSet
}
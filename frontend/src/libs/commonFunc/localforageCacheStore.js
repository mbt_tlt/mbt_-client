import LocalForage from 'localforage'

/**
 * 基于 localforage 开发的缓存数据仓库
 */
export class LocalForageCacheStore {
    /**
     * 构造方法
     */
    constructor() {}

    /**
     * 缓存数据仓库对象管理器, 用于管理各个不同的数据库
     */
    cacheStoreManager = new Map()

    /**
     * 是否存在指定数据库
     *
     * @param { string } databaseKey - 数据库索引
     * @return { boolean } 是否存在指定数据库
     */
    hasDatabase(databaseKey) {
        return this.cacheStoreManager.has(databaseKey)
    }

    /**
     * 新建数据库
     *
     * @param { string } databaseKey - 数据库索引
     * @return { boolean } 是否成功创建数据库
     */
    createDatabase(databaseKey) {
        if (this.hasDatabase(databaseKey)) {
            return false
        }

        // 新建数据库对象并将其添加进数据库管理器
        this.cacheStoreManager.set(databaseKey, {
            databaseInstance: LocalForage.createInstance({
                name: databaseKey,
            }),
            dataInstanceMap: new Map(),
        })
            return true
        }

    /**
     * 销毁数据库
     *
     * @param { string } databaseKey - 数据库索引
     * @param { Function } successCallback - 成功回调函数, 可选参数
     * @param { Function } failCallback - 失败回调函数, 可选参数
     * @return { Promise<boolean> } 是否成功销毁数据库
     */
    async destroyDatabase(
        databaseKey,
        successCallback = () => {},
        failCallback = () => {},
    ) {
        if (!this.hasDatabase(databaseKey)) {
            return false
        }

        this.cacheStoreManager.delete(databaseKey)
        await LocalForage.dropInstance({
            name: databaseKey,
        }).then(successCallback).catch(failCallback)

        return true
    }

    /**
     * 重置数据库
     *
     * @param { string } databaseKey - 数据库索引
     * @param { Function } successCallback - 成功回调函数, 可选参数
     * @param { Function } failCallback - 失败回调函数, 可选参数
     * @return { Promise<boolean> } 是否成功重置数据库
     */
    async reSetDatabase(
        databaseKey,
        successCallback = () => {},
        failCallback = () => {},
    ) {
        const cacheStoreObj = this.cacheStoreManager.get(databaseKey)
        if (!cacheStoreObj) {
            return false
        }

        await cacheStoreObj.databaseInstance.clear().then(successCallback).catch(failCallback)

        return true
    }

    /**
     * 是否存在指定数据实例
     *
     * @param { string } databaseKey - 数据表所属数据库索引
     * @param { string } dataInstanceKey - 数据实例索引
     * @return { boolean } 是否存在指定数据表
     */
    hasDataInstance(databaseKey, dataInstanceKey) {
        if (!this.hasDatabase(databaseKey)) {
            return false
        }

        const cacheStoreObj = this.cacheStoreManager.get(databaseKey)
        if (!cacheStoreObj) {
            return false
        }

        return cacheStoreObj.dataInstanceMap.has(dataInstanceKey)
    }

    /**
     * 设置数据实例
     *
     * @param { string } databaseKey - 数据实例所属数据库索引
     * @param { string } dataInstanceKey - 数据实例索引
     * @param { any } dataInstance - 数据实例对象
     * @param { number } failureTime - 失效时间, 默认一天, 单位秒
     * @param { Function } successCallback - 成功回调函数, 可选参数
     * @param { Function } failCallback - 失败回调函数, 可选参数
     */
    async setDataInstance(
        databaseKey,
        dataInstanceKey,
        dataInstance,
        failureTime = 86400,
        successCallback = () => {},
        failCallback = () => {},
    ) {
        if (!this.hasDatabase(databaseKey)) {
            return
        }

        const cacheStoreObj = this.cacheStoreManager.get(databaseKey)
        if (!cacheStoreObj) {
            return
        }

        await cacheStoreObj.databaseInstance.getItem(dataInstanceKey).then(async (getResult) => {
            if (getResult === null) {
                // 新建逻辑
                await cacheStoreObj.databaseInstance.setItem(dataInstanceKey, dataInstance)
                    .then((setResult) => {
                        const nowDate = new Date()

                        cacheStoreObj.dataInstanceMap.set(dataInstanceKey, {
                            data: dataInstance,
                            createDate: nowDate,
                            updateDate: nowDate,
                            failureDate: new Date(nowDate.getTime() + failureTime * 1000),
                        })

                        successCallback(setResult)
                    })
                    .catch(failCallback)
            } else {
                // 更新逻辑
                await cacheStoreObj.databaseInstance.setItem(dataInstanceKey, dataInstance)
                    .then((setResult) => {
                        cacheStoreObj.dataInstanceMap.set(dataInstanceKey, {
                            data: dataInstance,
                            createDate: getResult.createDate,
                            updateDate: new Date(),
                            failureDate: getResult .failureDate,
                        })

                        successCallback(setResult)
                    })
                    .catch(failCallback)
            }
        })
    }

    /**
     * 删除数据实例
     *
     * @param { string } databaseKey - 数据表所属数据库索引
     * @param { string } dataInstanceKey - 数据实例索引
     * @param { Function } successCallback - 成功回调函数, 可选参数
     * @param { Function } failCallback - 失败回调函数, 可选参数
     */
    async removeDataInstance(
        databaseKey,
        dataInstanceKey,
        successCallback = () => {},
        failCallback = () => {},
    ) {
        if (!this.hasDatabase(databaseKey)) {
            return
        }

        const cacheStoreObj = this.cacheStoreManager.get(databaseKey)
        if (!cacheStoreObj) {
            return
        }

        await cacheStoreObj.databaseInstance
            .getItem(dataInstanceKey)
            .then(async (result) => {
                await cacheStoreObj.databaseInstance
                    .removeItem(dataInstanceKey)
                    .then(() => {
                        cacheStoreObj.dataInstanceMap.delete(dataInstanceKey)
                        successCallback(result)
                    })
                    .catch(failCallback)
            })
            .catch(failCallback)
    }

    /**
     * 查询数据实例
     *
     * @param { string } databaseKey - 数据表所属数据库索引
     * @param { string } dataInstanceKey - 数据实例索引
     * @param { Function } successCallback - 成功回调函数, 可选参数
     * @param { Function } failCallback - 失败回调函数, 可选参数
     */
    async getDataInstance(
        databaseKey,
        dataInstanceKey,
        successCallback = () => {},
        failCallback = () => {},
    ) {
        if (!this.hasDatabase(databaseKey)) {
            return
        }

        const cacheStoreObj = this.cacheStoreManager.get(databaseKey)
        if (!cacheStoreObj) {
            return
        }

        await cacheStoreObj.databaseInstance.getItem(dataInstanceKey).then(successCallback).catch(failCallback)
    }
}
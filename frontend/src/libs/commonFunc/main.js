// 时间序列化通用函数
import { timeTextual, inverseTimeTextual } from "@/libs/commonFunc/timeTextual";
// AJAX 通用函数
import {
    getCookie,
    createXMLHttpRequest,
    convertDataAJAX,
    anomalyCaptureAJAX
} from "@/libs/commonFunc/ajax";
// i18n 通用函数
import { getI18nObj, i18n } from "@/libs/commonFunc/i18n";


/**
 * haveTargetNode - 查询基准标签的 DOM Node 链上是否存在指定标签
 *
 * @const
 * @export
 * @function
 * @param { Element } benchmarkNode - 基准标签节点
 * @param { string } attributeValue - 查询属性值
 * @param { string } [attribute = 'id'] - 查询依据属性
 * @return { Element|undefined } 若查询到指定标签则返回, 否则返回 void 0
 * */
export const haveTargetNode = function (
    benchmarkNode,
    attributeValue,
    attribute = 'id'
) {
    let targetNode = benchmarkNode;
    while (targetNode !== null) {
        if (
            targetNode.getAttribute &&
            targetNode.getAttribute(attribute) === attributeValue
        ) {
            return targetNode;
        }

        targetNode = targetNode.parentNode;
    }

    return void 0;
}

/**
 * weightRand - 依据权重选取
 *
 * @const
 * @export
 * @function
 * @param { Array<Object> } itemList - 形如 { weight: number ... } 对象数组
 * @return { Object<any> } 返回形如 { item: Object<any>, index: number } 的结果对象
 * */
export const weightRand = function (itemList) {
    // 为列表项打上权重选区标记并计算总权重
    const totalWeight = itemList.reduce(function (preVal, targetItem, itemIndex) {
        targetItem.start = preVal;

        return targetItem.end = preVal + targetItem.weight;
    }, 0);

    // 依据随机数进行权重选区选取
    const random = Math.floor(Math.random() * totalWeight);
    const selectItemIndex = itemList.findIndex((item) => {
        return item.start <= random && item.end > random;
    });

    return {
        index: selectItemIndex,
        item: itemList[selectItemIndex]
    };
};

/**
 * scrollGradualRoll - scroll 渐变滚动
 *
 * @const
 * @export
 * @function
 * @param { Element } targetDom - 目标标签对象
 * @param { number } distance - 滚动总距离
 * @param { boolean } [isVertical = true] - 是否为垂直滚动条
 * @param { boolean } [isImmediately = false] - 是否立即执行
 * @param { number } [time = 100] - 滚动时间 ms
 * */
export const scrollGradualRoll = function (
    targetDom,
    distance,
    isVertical = true,
    isImmediately = false,
    time = 100,
) {
    /**
     * scrollRoll - scroll 滚动
     *
     * @const
     * @function
     * @memberOf scrollGradualRoll
     * @param { number } rollDistance - 滚动距离
     * */
    const scrollRoll = function (rollDistance) {
        // 是否为垂直滚动
        if (isVertical) {
            targetDom.scrollTop += rollDistance;
        } else {
            targetDom.scrollLeft += rollDistance ;
        }
    }

    // 特色情况处理（若小于步进时间则立即执行）
    if (isImmediately) {
        scrollRoll(distance);
        return;
    }

    // 步进缓动
    const stepTime = 16.6;
    // 参数校准（若小于步进时间则置于最小步进时间）
    if (time < stepTime) {
        time = stepTime;
    }
    const stepNum = Math.floor(time / stepTime);
    const stepDistance = Math.floor(distance / stepNum);

    let executedStepNum = 0;
    const stepRoll = setInterval(() => {
        // 清除步进缓动
        if (executedStepNum >= stepNum) {
            clearInterval(stepRoll);
            return;
        }

        scrollRoll(stepDistance);

        executedStepNum++;
        // 若为最后一次步进缓动 则 触发延时补齐缓动
        if (executedStepNum + 1 === stepNum) {
            const calibrateDistance = distance - (stepNum * stepDistance);
            const calibrateTime = time - (stepNum * stepTime);

            setTimeout(() => {
                scrollRoll(calibrateDistance);
            }, calibrateTime);
        }
    }, stepTime);
}

/**
 * getTrueElementWidth - 获取指定 HTML 元素的真实宽度
 *
 * @const
 * @export
 * @function
 * @param { Element } componentElement - 组件 DOM 对象
 * @return { number } 真实宽度
 */
export const getTrueElementWidth = function (componentElement) {
    const computedStyle = getComputedStyle(componentElement, null);

    const trueWidth = componentElement.offsetWidth
        + Number(computedStyle.marginLeft.replace(/[^0-9]/g,''))
        + Number(computedStyle.marginRight.replace(/[^0-9]/g,''))
        + Number(computedStyle.borderLeftWidth.replace(/[^0-9]/g,''))
        + Number(computedStyle.borderRightWidth.replace(/[^0-9]/g,''));

    return Math.ceil(trueWidth);
}

/**
 * deepCopy - 深拷贝
 *
 * @const
 * @export
 * @function
 * @param { any } value - 拷贝对象
 * */
export const deepCopy = function (value) {
    const valueType = Object.prototype.toString.call(value);

    switch (valueType) {
        case '[object Object]':
            const newValueObj = {};
            for (let key in value) {
                newValueObj[key] = deepCopy(value[key]);
            }

            return newValueObj;
        case '[object Array]':
            const newValueArr = [];
            for (let num = 0; num < value.length; num++) {
                newValueArr[num] = deepCopy(value[num]);
            }

            return newValueArr;
        default:
            return value;
    }
}

/**
 * uuidMaker - uuid 生成器
 *
 * @const
 * @export
 * @function
 * @param { Number } [len = 32] - uuid 长度, 默认为 32 位
 * @param { Number } [radix = 90] - uuid 基准数, 即采用密文表前多少位元素做为密文元素
 * */
export const uuidMaker = function (len = 32, radix = 90) {
    /**
     * chars - 密文表数组
     * */
    const chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz~!@#$%^&*()_+-=<>?,./[]{};:'.split('');

    // 保证基准数为整数
    radix = Math.floor(radix);
    radix = radix || chars.length;

    const uuid = [];
    for (let num = 0; num < len; num++) {
        uuid[num] = chars[0 | Math.random() * radix];
    }

    return uuid.join('');
}

/**
 * getParamType - 获取参数类型标识
 *
 * @const
 * @export
 * @function
 * @param { any } param - 待检测类型参数
 * @return { string } 参数类型字符串
 * */
export const getParamType = function (param) {
    return Object.prototype.toString.call(param).match(/^\[object\s(.*)]$/)[1];
}

export default {
    haveTargetNode,
    weightRand,
    scrollGradualRoll,
    getTrueElementWidth,
    deepCopy,
    uuidMaker,
    getParamType,
    // 时间序列化相关
    timeTextual,
    inverseTimeTextual,
    // AJAX 相关
    getCookie,
    createXMLHttpRequest,
    convertDataAJAX,
    anomalyCaptureAJAX,
    // i18n 相关
    getI18nObj,
    i18n
}

/**
 * getTimeObj - 获得时间对象
 *
 * @export
 * @const
 * @function
 * @param { Date } [date = new Date()] - Date 对象
 * @return { Object } 时间对象
 * */
export const getTimeObj = function (date = new Date()) {
    return {
        year: date.getFullYear(),
        month: date.getMonth() + 1,
        day: date.getDate(),
        hour: date.getHours(),
        minute: date.getMinutes(),
        second: date.getSeconds(),
        millisecond: date.getMilliseconds()
    }
}

/**
 * getTimePeriod - 获得时间段
 *
 * @export
 * @const
 * @function
 * @param { Date } [date = new Date()] - Date 对象
 * @return { string } 时间段字符
 * */
export const getTimePeriod = function (date = new Date()) {
    const hours = date.getHours();

    if (hours < 5) {
        // 后半夜 0:00 ~ 4:59
        return 'lateNight';
    } else if (5 <= hours && hours < 9) {
        // 清晨 5:00 ~ 8:59
        return 'earlyMorning';
    } else if (9 <= hours && hours < 12) {
        // 上午 9:00 ~ 11:59
        return 'morning';
    } else if (12 <= hours && hours < 14) {
        // 中午 12:00 ~ 13:59
        return 'noon';
    } else if (14 <= hours && hours < 17) {
        // 下午 14:00 ~ 16:59
        return 'afternoon';
    } else if (17 <= hours && hours < 19) {
        // 傍晚 17:00 ~ 18:59
        return 'evening';
    } else if (19 <= hours && hours < 21) {
        // 入夜 19:00 ~ 20:59
        return 'intoNight';
    } else if (21 <= hours && hours < 24) {
        // 深夜 21:00 ~ 23:59
        return 'deepNight';
    } else {
        // 未知时间, 解析错误
        return 'unknown';
    }
}

/**
 * stitchingTimeStr - 拼接时间字符串
 *
 * @export
 * @const
 * @function
 * @param { string } timeStateKey - 时间状态索引
 * @param { Array<Object> } timeArr - 时间对象数组
 * @param { string } [separator = ':'] - 分隔符
 * @return { string } 时间字符串
 * */
export const stitchingTimeStr = function (
    timeStateKey,
    timeArr,
    separator = ':'
) {
    let reStr = '';
    timeArr.forEach((timeItem) => {
        if (timeItem.timeStateKey === timeStateKey) {
            reStr += `${ timeItem.value }${ separator }`;
        }
    });
    // 去除最后一个字符, 即去除最后多添加的那个分隔符
    reStr = reStr.substring(0, reStr.length - 1);

    return reStr;
}

/**
 * timeTextual - 时间文本化
 *
 * @const
 * @export
 * @function
 * @param { number } duration - 时长
 * @param { boolean } [isRough = true] - 是否去除毫秒数
 * @param { string } [separator = ':'] - 分隔符
 * @return { string } 文本化时间
 * */
export const timeTextual = function (
    duration,
    isRough = true,
    separator = ':'
) {
    if (duration < 0 || isNaN(duration)) {
        return isRough ? `00${ separator }00` : `00${ separator }00${ separator }000`;
    }

    // 参数校验
    if (isRough) {
        duration = Math.floor(duration);
    }

    // 小时位
    const hour = Math.floor(duration / 3600);
    const hourText = hour < 10 ? `0${ hour }` : `${ hour }`;

    // 分钟位
    const min = Math.floor(duration / 60) % 60;
    const minText = min < 10 ? `0${ min }` : `${ min }`;

    // 秒位
    const sec = Math.floor(duration) % 60;
    const secText = sec < 10 ? `0${ sec }` : `${ sec }`;

    // 毫秒位
    const millisecond = duration - sec;
    const millisecondText = millisecond.toFixed(3).substring(2);

    // 粗略时间文本（ 时分秒 或 时分 ）
    const roughTimeStr = hour !== 0
        ? `${ hourText }${ separator }${ minText }${ separator }${ secText }`
        : `${ minText }${ separator }${ secText }`;

    return isRough ? roughTimeStr : roughTimeStr + `${ separator }${ millisecondText }`;
}

/**
 * inverseTimeTextual - 逆时间文本化 / 文本时间数字化
 *
 * @const
 * @export
 * @function
 * @param { string } timeText - 文本时长
 * @param { boolean } [isRough = false] - 是否去除毫秒数
 * @param { string } [separator = ':'] - 分隔符
 * @return { number } 数字化时间
 * */
export const inverseTimeTextual = function (
    timeText,
    isRough = false,
    separator = ':'
) {
    const timeScrapArr = timeText.split(separator);
    const timeScrapArrLen = timeScrapArr.length;

    // 秒数据组（秒, 毫秒）
    const second = Number(timeScrapArr[timeScrapArrLen - 2]);

    let millisecond = 0;
    // 若存在小数点（即包含小数位）且不去除毫秒数
    if (!isRough) {
        millisecond = Number(timeScrapArr[timeScrapArrLen - 1]) / 1000;
    }

    // 总时间（秒）
    let totalTime = second + millisecond;
    for (let num = timeScrapArrLen - 3, step = 1; num >= 0; num--, step++) {
        totalTime = totalTime + timeScrapArr[num] * Math.pow(60, step);
    }

    return Number(totalTime.toFixed(3));
}

export default {
    getTimeObj,
    getTimePeriod,
    stitchingTimeStr,
    timeTextual,
    inverseTimeTextual,
}

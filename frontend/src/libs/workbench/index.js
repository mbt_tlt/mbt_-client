/**
 * saveDownload - 文件保存下载
 *
 * @function
 * @param { String|Blob } content - 保存内容
 * @param { String } filename - 文件名
 * @return { Blob } 文件 Blob 对象
 * */
export const saveDownload = function (content, filename) {
    const saveBufferEle = document.createElement('a');
    saveBufferEle.download = filename;
    saveBufferEle.style.display = 'none';

    const file =
        Object.prototype.toString.call(content) === '[object Blob]'
        ? content
        : new Blob([content]);
    saveBufferEle.href = URL.createObjectURL(file);

    // 触发 a标签 保存逻辑
    document.body.appendChild(saveBufferEle);
    saveBufferEle.click();
    document.body.removeChild(saveBufferEle);

    return file;
};

/**
 * fileValidityCheck - 文件合法性检测
 *
 * @function
 * @param { Array<File> } fileArr - 文件列表
 * @param { Object } fileValidityCheckObj - 指定文件合法性检测对象, key - 指定文件后缀名, value - 合法性检测方法
 * @throws { TypeError } 合法性检测失败
 * */
const fileValidityCheck = function (fileArr, fileValidityCheckObj) {
    Array.from(fileArr).forEach((loadFile) => {
        // 文件后缀判断
        const fileNameDebris = loadFile.name.split('.');
        const fileSuffixName = fileNameDebris.slice(1).join('.');

        if (fileValidityCheckObj.hasOwnProperty(fileSuffixName)) {
            const fileReader = new FileReader();
            fileReader.readAsText(loadFile);
            fileReader.onload = function () {
                const loadFileContent = fileReader.result;

                // 这里要将 loadFileContent 过一个文件有效性验证
                const detectionResult = fileValidityCheckObj[fileSuffixName](loadFileContent);
                if (detectionResult) {
                    throw TypeError(`检测到 ${ loadFile.name } 文件内容异常修改或损坏`);
                }
            };
        } else {
            throw TypeError(`${ loadFile.name } 文件格式有误! 请上传格式符合 ${ Object.keys(fileValidityCheckObj).join(', ') } 的文件`);
        }
    });
};

export default {
    saveDownload,
    fileValidityCheck
}

import { createApp } from 'vue';
// 引入主组件
import VueApp from './App.vue';
// 引入 CSS 字体库
import '@assets/style/font-awesome.css';
// 引入 vue-router
import router from '@/router/router.js';
// 引入 Axios
import axios from "axios";
// 引入 i18n
import { i18n, getI18nObj } from "@/libs/commonFunc/i18n";
// 引入 element-plus 及样式
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
// 引入 element-plus 图标资源
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
// 创建 vue 主服务
const app = createApp(VueApp);

// 挂载 vue-router
app.use(router);

// 挂载 element-plus
app.use(ElementPlus);

// 注入 Axios
app.provide('$axios', axios);
// 注入 i18n 插件
app.provide('$i18n', i18n);
app.provide('$getI18nObj', getI18nObj);

// 注册所有 Element 图标组件
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(`el-icon-${ key }`, component);
}

// 挂载 Vue 实例
app.mount('#mbt_client');

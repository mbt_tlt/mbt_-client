import { createRouter, createWebHistory } from "vue-router";

import homePage from "@/pages/home/Home.vue";
import userPage from "@/pages/user/User.vue";
import playListPage from "@/pages/playList/PlayList.vue";
import playList_CardPage from "@/pages/playList/cardChildPage/CardChildPage.vue";
import playList_ListPage from "@/pages/playList/listChildPage/ListChildPage.vue";
import workBenchPage from "@/pages/workbench/Workbench.vue";
import settingPage from "@/pages/setting/Setting.vue";
import notFoundPage from "@/pages/notFound/NotFound.vue";

/**
 * getRouterPath - 获取 router 路由配置
 *
 * @export
 * @const
 * @function
 * @param { string } routerName - 路由别名
 * @return { Object } 路由对象
 * */
export const getRouterPath = function (routerName) {
    return routerConfig.filter((item) => {
        return item.name === routerName;
    });
}

/**
 * routerConfig - vue-router 配置表
 *
 * @export
 * @const
 * @type Object
 * */
export const routerConfig = [
    {
        path: '/',
        redirect: '/home'
    },
    // 主页面
    {
        path: '/home',
        name: 'Home',
        component: homePage,
    },
    // 用户信息
    {
        path: '/user',
        name: 'User',
        component: userPage,
    },
    // 歌单列表
    {
        path: '/playList',
        name: 'PlayList',
        component: playListPage,
        redirect: '/playList/card',
        children: [
            {
                path: 'card',
                name: 'PlayList-Card',
                component: playList_CardPage
            },
            {
                path: 'list',
                name: 'PlayList-List',
                component: playList_ListPage
            }
        ]
    },
    // 音频工作台
    {
        path: '/workbench',
        name: 'Workbench',
        component: workBenchPage,
    },
    // 设置中心
    {
        path: '/setting',
        name: 'Setting',
        component: settingPage,
    },
    // 匹配所有没有显式定义过的路由, 显示 404 Not Found
    {
        path: '/:pathMatch(.*)*',
        name: 'Not Found',
        component: notFoundPage
    },
]

const router = createRouter({
    history: createWebHistory(),
    routes: routerConfig
});

export default router;

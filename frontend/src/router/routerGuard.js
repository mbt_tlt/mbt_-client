/**
 * routerBeforeEach - router.beforeEach router 前置守卫函数
 *
 * @export
 * @const
 * @function
 * @param to - 当前导航即将要进入的目标路由
 * @param from - 当前导航正要离开的路由
 * @param next - next 指定跳转路由控制器
 * */
export const routerBeforeEach = function (to, from, next) {
    
}

/**
 * routerBeforeResolve - router.beforeResolve router 前置解析函数
 *
 * @export
 * @const
 * @function
 * @param to - 当前导航即将要进入的目标路由
 * @param from - 当前导航正要离开的路由
 * @param next - next 指定跳转路由控制器
 * */
export const routerBeforeResolve = function (to, from, next) {
    
}

/**
 * routerAfterEach - router.afterEach router 后置钩子函数
 *
 * @export
 * @const
 * @function
 * @param to - 当前导航即将要进入的目标路由
 * @param from - 当前导航正要离开的路由
 * */
export const routerAfterEach = function (to, from) {

}

export default {
    routerBeforeEach,
    routerBeforeResolve,
    routerAfterEach
}

import { reactive } from 'vue';


/**
 * mainStore - 主 store 对象
 *
 * @const
 * @export
 * @type Object
 * @example { boolean } mainStore.debug - 是否为调试模式
 * @example { Object } mainStore.state - 响应式对象集合对象
 * @example { Object } mainStore.getters - 响应式对象 get 方法集合对象
 * @example { Object } mainStore.setters - 响应式对象 set 方法集合对象
 * @example { Object } mainStore.module - 响应式对象模块
 * */
export const mainStore = {
    // 是否允许调试
    debug: false,
    // 响应式对象集合对象
    state: reactive({

    }),
    // 响应式对象 get 方法集合对象
    getters: {

    },
    // 响应式对象 set 方法集合对象
    setters: {

    },
    // 响应式对象模块
    module: {

    }
}

export default mainStore;

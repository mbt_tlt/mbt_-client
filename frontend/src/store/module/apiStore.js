import { reactive } from 'vue';
import axios from "axios";


/**
 * apiStore - api store 对象
 *
 * @const
 * @export
 * @type Object
 * @example { boolean } apiStore.debug - 是否为调试模式
 * @example { Object } apiStore.state - 响应式对象集合对象
 * @example { Object } apiStore.getters - 响应式对象 get 方法集合对象
 * @example { Object } apiStore.setters - 响应式对象 set 方法集合对象
 * @example { Object } apiStore.module - 响应式对象模块
 * */
export const apiStore = {
    // 是否允许调试
    debug: false,
    // 响应式对象集合对象
    state: reactive({
        /**
         * serverObj - 服务对象, 记录所有对接服务, key - 服务标记
         * */
        serverObj: {
            // MBT 主服务
            'mb_main_project': {
                baseUrl: 'https://www.mbt.top'
            },
            // MBT 自动化服务
            'MBT_Automation': {
                baseUrl: 'https://www.mbt.top'
            }
        },
        /**
         * apiObj - api 对象
         * */
        apiObj: {
            // key 对应 服务标记, value 对应 服务 api 对象组
            'mb_main_project': {
                // 新建歌单
                'createPlaylist': {
                    url: '/playlist/{version}/create',
                    param: {
                        /**
                         * standard - 文档规范的请求参数对象
                         * */
                        standard: {
                            // 接口版本号
                            version: {
                                // 记录该请求参数属于何处
                                source: 'path',
                                // 数据类型
                                type: 'String',
                            },
                            // 用户 token 标识
                            token: {
                                source: 'header',
                                type: 'String',
                            },
                        },
                        /**
                         * checkMethod - 参数合法性校验方法
                         *
                         * @function
                         * @param { Object } standard - 文档规范请求参数对象
                         * @param { Object } param - 实际请求参数对象
                         * @return { Object } 是否通过检验
                         * */
                        checkMethod: function (standard, param) {
                            const failReasonArr = [];

                            // 校验 param 合法性
                            const standardKeyArr = Object.keys(standard);
                            for (let itemKey of standardKeyArr) {
                                // 判断是否缺失参数
                                if (!param.hasOwnProperty(itemKey)) {
                                    failReasonArr.push(`Error: 缺失参数 ${ itemKey }`);
                                    continue;
                                }
                                // 判断来源是否正确
                                if (param[itemKey].source !== standard[itemKey].source) {
                                    failReasonArr.push(`Error: 参数 ${ itemKey } 来源 ${ param[itemKey].source } 与文档规范来源 ${ standard[itemKey].source } 不符`);
                                }
                            }

                            return {
                                value: failReasonArr.length === 0,
                                // 异常原因
                                failReasonArr: failReasonArr
                            };
                        }
                    },
                    data: {
                        standard: {
                            playlistArr: []
                        },
                        /**
                         * checkMethod - 参数合法性校验方法
                         *
                         * @function
                         * @param { Object } standard - 文档规范对象
                         * @param { Object } data - 实际对象
                         * @return { Object } 是否通过检验
                         * */
                        checkMethod: function (standard, data) {
                            const failReasonArr = [];

                            // 校验 data 合法性
                            const standardKeyArr = Object.keys(standard);
                            for (let itemKey of standardKeyArr) {
                                // 判断是否缺失参数
                                if (!data.hasOwnProperty(itemKey)) {
                                    failReasonArr.push(`Error: 缺失参数 ${ itemKey }`);
                                }
                            }

                            return {
                                value: failReasonArr.length === 0,
                                // 异常原因
                                failReasonArr: failReasonArr
                            };
                        }
                    },
                    method: 'GET',
                    header: {
                        'content-type': 'application/x-www-form-urlencoded'
                    }
                },
            }
        }
    }),
    // 响应式对象 get 方法集合对象
    getters: {

    },
    // 响应式对象 set 方法集合对象
    setters: {
        /**
         * universalAPIMethod - 泛用 API 通讯方法
         *
         * @function
         * @param { string } serverKey - 服务索引
         * @param { string } apiKey - API 索引
         * @param { Object } param - 请求参数对象
         * @param { Object } [data = {}] - 请求实体对象
         * @param { Object } [header = {}] - 请求头对象, 该对象为增量添加, 真实 axios 携带的 header 内容为规范 header + header param + 该参数共同组成的一个对象, 重复时则会取用户自定义值覆盖
         * @param { Function } [successMethod = function (res) {}] - 成功回调处理函数
         * @param { Function } [failMethod = function (err) {}] - 成功回调处理函数
         * @param { Function } [finallyMethod = function () {}] - 回调处理函数
         * */
        universalAPIMethod: function (
            serverKey,
            apiKey,
            param,
            data = {},
            header= {},
            successMethod = function (res) {},
            failMethod = function (err) {},
            finallyMethod = function () {}
        ) {
            const paramKeyArr = Object.keys(param);
            const headerKeyArr = Object.keys(header);

            // 构建检测类别索引数组, 并遍历校验各项类别合法性
            const checkKeyArr = [
                { key: 'param', value: param },
                { key: 'data', value: data },
            ];
            checkKeyArr.forEach((checkConfigObj) => {
                const checkResult = apiStore.state.apiObj[serverKey][apiKey][checkConfigObj.key].checkMethod(
                    apiStore.state.apiObj[serverKey][apiKey][checkConfigObj.key].standard,
                    checkConfigObj.value
                );

                if (checkResult.value === false) {
                    // 检验失败原因对象转字符串
                    let failReasonStr = '';
                    checkResult.failReasonArr.forEach((item, index) => {
                        failReasonStr += `\n\t${ index + 1 }. ${ item }`;
                    });

                    throw RangeError(`${ serverKey } 服务 ${ apiKey } 接口 ${ checkConfigObj.key } 参数异常, 原因为: ${ failReasonStr }`);
                }
            });

            // 构建并正则替换 url 中所有 path param 项
            let url = apiStore.state.apiObj[serverKey][apiKey].url;
            paramKeyArr.filter((paramKey) => {
                return param[paramKey].source === 'path';
            }).forEach((paramKey) => {
                const reg = new RegExp(`{${ paramKey }}`, 'g');
                url = url.replaceAll(reg, param[paramKey].value);
            });

            // 构建真实的 param 与 header 参数
            const realParam = {};
            paramKeyArr.filter((paramKey) => {
                return param[paramKey].source === 'query';
            }).forEach((paramKey) => {
                realParam[paramKey] = param[paramKey].value;
            });

            // 构建真实的 header 参数
            const realHeader = JSON.parse(JSON.stringify(apiStore.state.apiObj[serverKey][apiKey].header));
            paramKeyArr.filter((paramKey) => {
                return param[paramKey].source === 'header';
            }).forEach((headerKey) => {
                realHeader[headerKey] = param[headerKey].value;
            });
            headerKeyArr.forEach((headerKey) => {
                realHeader[headerKey] = headerKeyArr[headerKey];
            });

            // 构建并执行 Axios 通讯方法及相关回调处理
            axios({
                url: url,
                baseURL: apiStore.state.serverObj[serverKey].baseUrl,
                method: apiStore.state.apiObj[serverKey][apiKey].method,
                params: realParam,
                data: data,
                headers: realHeader
            }).then(successMethod).catch(failMethod).finally(finallyMethod);
        }
    },
    // 响应式对象模块
    module: {

    }
}

export default apiStore;

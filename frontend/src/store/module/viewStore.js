import { reactive } from 'vue';
import { getRouterPath } from "@/router/router";
import { i18n } from "@/libs/commonFunc/i18n";

/**
 * viewStore - 视图 store 对象
 *
 * @const
 * @export
 * @type Object
 * @example { boolean } viewStore.debug - 是否为调试模式
 * @example { Object } viewStore.state - 响应式对象集合对象
 * @example { Object } viewStore.getters - 响应式对象 get 方法集合对象
 * @example { Object } viewStore.setters - 响应式对象 set 方法集合对象
 * @example { Object } viewStore.module - 响应式对象模块
 * */
export const viewStore = {
    // 是否允许调试
    debug: false,
    // 响应式对象集合对象
    state: reactive({
        // 用户设置视图对象
        userSettingView: {
            // 唯一性索引
            key: 'userSettingView',
            // 名称简写
            abbreviatedName: i18n('viewStore-userSettingView-abbreviatedName'),
            // 定位
            position: 'top',
            // fontawesome 图标样式
            iconStyle: 'fa-user-o',
            // 图片路径
            imgSrc: '',
            // 提示框内容
            tooltip: '',
            // 路由
            href: getRouterPath('User')[0].path || '',
            // 路由别名
            hrefName: 'User',
            // 路由参数
            hrefParams: {},
        },
        // 歌单视图对象
        playlistView: {
            key: 'playlistView',
            abbreviatedName: i18n('viewStore-playlistView-abbreviatedName'),
            position: 'center',
            iconStyle: 'fa-music',
            imgSrc: '',
            tooltip: '',
            href: getRouterPath('PlayList')[0].path || '',
            hrefName: 'PlayList',
            hrefParams: {},
        },
        // 音频工作台视图对象
        workbenchView: {
            key: 'workbenchView',
            abbreviatedName: i18n('viewStore-workbenchView-abbreviatedName'),
            position: 'center',
            iconStyle: 'fa-wrench',
            imgSrc: '',
            tooltip: '',
            href: getRouterPath('Workbench')[0].path || '',
            hrefName: 'Workbench',
            hrefParams: {},
        },
        // 设置视图对象
        settingView: {
            key: 'settingView',
            abbreviatedName: i18n('viewStore-settingView-abbreviatedName'),
            position: 'bottom',
            iconStyle: 'fa-cog',
            imgSrc: '',
            tooltip: '',
            href: getRouterPath('Setting')[0].path || '',
            hrefName: 'Setting',
            hrefParams: {},
        }
    }),
    // 响应式对象 get 方法集合对象
    getters: {

    },
    // 响应式对象 set 方法集合对象
    setters: {

    },
    // 响应式对象模块
    module: {

    }
}

export default viewStore;

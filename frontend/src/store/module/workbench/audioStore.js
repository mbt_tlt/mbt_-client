import { ref, reactive, nextTick } from 'vue';
import { ElMessage } from "element-plus";
import Peaks from "peaks.js";
import workbenchStore from "@/store/module/workbench/workbenchStore.js";

/**
 * audioStore - peaks 音频管理 store 对象
 *
 * @const
 * @export
 * @type Object
 * @example { boolean } audioStore.debug - 是否为调试模式
 * @example { Object } audioStore.state - 响应式对象集合对象
 * @example { Object } audioStore.getters - 响应式对象 get 方法集合对象
 * @example { Object } audioStore.setters - 响应式对象 set 方法集合对象
 * @example { Object } audioStore.module - 响应式对象模块
 * */
export const audioStore = {
    // 是否允许调试
    debug: false,
    // 响应式对象集合对象
    state: reactive({
        /**
         * audioMappingObj - 音频映射对象, 映射源 workbenchStore.state.projectObj.projectArr[xxx].data.audioData
         * */
        audioMappingObj: {
            '工程1': {
                src: '/public/audio/mothy - 第1幕1曲　Queen Of The Glass.mp3',
                // audio 的 arrayBuffer 文件
                audioArrayBuffer: null,
                // audio 的 arrayBuffer 解码数据
                decodeAudio: null,
                // audio 播放进度
                currentTime: 0
            }
        },
        /**
         * waveformOption - peaks 波形图组件配置对象
         * */
        waveformOption: ref({
            // 波形图是否在加载中
            loading: true,
            // audio 通用配置项目
            audioOption: {
                // 音频播放状态
                playbackState: false,
            },
            // peaks 实例
            peaks: null,
            // peaks 通用配置项
            peaksOption: {
                // 波形图缩放配置
                zoomview: {
                    // zoomView 虚拟 Dom 对象
                    container: ref(null)
                },
                // 波形图概略配置
                overview: {
                    // overView 虚拟 Dom 对象
                    container: ref(null),
                },
                // 绑定的 audio 元素 Dom 对象
                mediaElement: ref(null),
                // 音频处理器配置
                webAudio: {
                    audioContext: new AudioContext(),
                },
                // 是否显示提示信息
                emitCueEvents: true,
            }
        }),
    }),
    // 响应式对象 get 方法集合对象
    getters: {
        /**
         * getTargetAudioMappingObj - 获得指定 audio 映射对象, 默认获取当前活动工程所属对象
         *
         * @function
         * @param { string } [targetKey = void 0] - 指定 audio 映射对象索引
         * */
        getTargetAudioMappingObj: function (targetKey = void 0) {
            return targetKey !== void 0
                ? audioStore.state.audioMappingObj[targetKey]
                : audioStore.state.audioMappingObj[workbenchStore.state.projectObj.activeProjectKey];
        },
        /**
         * getAudioDataInformation - 获取当前音频
         *
         * @function
         * @return { Object } 音频信息对象
         * */
        getAudioDataInformation: function () {
            const peaksInstance = audioStore.state.waveformOption.peaks;

            return {
                // 当前播放指针所在进度时间
                currentTime: peaksInstance.player.getCurrentTime(),
                // 当前音频总时长
                duration: peaksInstance.player.getDuration()
            }
        }
    },
    // 响应式对象 set 方法集合对象
    setters: {
        /**
         * addAudioMappingObjItem - 创建音频映射源对象
         *
         * @function
         * @param { string } key - 映射源对象索引
         * @param { Object } value - 映射源音频对象
         * */
        addAudioMappingObjItem: function (key, value) {
            // 首先判断指定索引是否存在
            if (audioStore.state.audioMappingObj[key] !== void 0) {
                throw Error('指定音频映射源对象已存在!');
            }
            // 创建
            audioStore.state.audioMappingObj[key] = value;
        },
        /**
         * deleteSubtitleMappingObjItem - 销毁音频映射源对象
         *
         * @function
         * @param { string } key - 映射源对象索引
         * */
        deleteAudioMappingObjItem: function (key) {
            // 首先判断指定索引是否存在
            if (audioStore.state.audioMappingObj[key] === void 0) {
                throw Error('指定音频映射源对象不存在!');
            }
            // 删除
            Reflect.deleteProperty(audioStore.state.audioMappingObj, key);
        },
        /**
         * peakEventHandlerBinding - peak 事件处理器绑定
         *
         * @function
         * @param { PeaksInstance } peaks - peaks 对象
         * */
        peakEventHandlerBinding: function (peaks) {
            /**
             * continuousEventHandlerObj - 持续型事件处理器
             * */
            const continuousEventHandlerObj = {
                // 音频暂停
                'player.pause': function (time) {
                    // 修改按钮样式
                    workbenchStore.setters.changeToolItemObj(
                        'lyricView',
                        'audioSwitch',
                        {
                            iconStyle: 'fa-play'
                        }
                    );
                },
                // 音频播放
                'player.playing': function (time) {
                    // 修改按钮样式
                    workbenchStore.setters.changeToolItemObj(
                        'lyricView',
                        'audioSwitch',
                        {
                            iconStyle: 'fa-pause'
                        }
                    );
                },
                // 音频指针时序变化
                'player.timeupdate': function (time) {
                    audioStore.setters.changeTargetAudioCurrentTime(time);
                },
                // 音频播放完毕
                'player.ended': function () {
                    // 修改按钮样式
                    workbenchStore.setters.changeToolItemObj(
                        'lyricView',
                        'audioSwitch',
                        {
                            iconStyle: 'fa-play'
                        }
                    );
                },
            }

            Object.keys(continuousEventHandlerObj).forEach((eventKey) => {
                peaks.on(eventKey, continuousEventHandlerObj[eventKey]);
            });
        },
        /**
         * initPeak - 初始化 Peak.js 音频波形图相关逻辑
         *
         * @function
         * */
        initPeak: function () {
            nextTick(() => {
                Peaks.init(audioStore.state.waveformOption.peaksOption, (err, peaks) => {
                    if (err) {
                        throw Error(`peak.js 初始化异常: ${ err.message }`);
                    }
                    // 挂载 peaks 实例
                    audioStore.state.waveformOption.peaks = peaks;
                    // todo 挂载 AudioBuffer 音频解码实例 mountDecodeAudioData, 所需的 arrayBuffer 是从创建工程时 input 读取文件, 并将其转化为 arrayBuffer 的
                    // audioStore.state.waveformOption.peaksOption.webAudio.audioContext.decodeAudioData(
                    //     workbenchStore.state.projectObj.projectArr[0].data.audioData.audioArrayBuffer
                    // ).then((decodedData) => {
                    //     // 挂载 decodeAudio 实例
                    //     console.log(decodedData)
                    // });

                    // 绑定事件处理器
                    audioStore.setters.peakEventHandlerBinding(peaks);

                    // 结束加载状态
                    audioStore.state.waveformOption.loading = false;
                });
            }).then(() => {

            });
        },
        /**
         * changePeaksSource - 切换 Peaks 组件链接音频
         *
         * @function
         * @param { string } mediaUrl - 媒体 url
         * */
        changePeaksSource: function (mediaUrl) {
            // 开启 loading 状态
            audioStore.state.waveformOption.loading = true;

            // 更新 peaks 组件链接音频源
            audioStore.state.waveformOption.peaks.setSource({
                mediaUrl: mediaUrl,
                webAudio: {
                    audioContext: audioStore.state.waveformOption.peaksOption.webAudio.audioContext
                }
            }, (err) => {
                if (err) {
                    ElMessage.error(`peaks 音频更新失败: ${ err.message }`);
                } else {
                    // todo 更新 peaks 组件所有记号点与记号段
                    // ...

                    // 关闭 loading 状态
                    audioStore.state.waveformOption.loading = false;
                }
            });
        },
        /**
         * changeTargetAudioCurrentTime - 变更指定音频映射对象播放进度
         *
         * @function
         * @param { number } time - 更新的时间值
         * @param { string } [targetKey = workbenchStore.state.projectObj.activeProjectKey] - 指定 audio 映射对象索引, 默认为当前活动工程所属 audio 映射对象
         * */
        changeTargetAudioCurrentTime: function (
            time,
            targetKey = workbenchStore.state.projectObj.activeProjectKey
        ) {
            audioStore.state.audioMappingObj[targetKey].currentTime = time;
        },
        /**
         * changeAudioPlaybackState - 改变音频播放状态
         *
         * @function
         * @param { boolean|undefined } [specifyState = void 0] - 指定音频播放状态状态
         * */
        changeAudioPlaybackState: function (specifyState = void 0) {
            const waveformOption = audioStore.state.waveformOption;

            if (specifyState !== void 0) {
                // 若有指定音频的播放状态
                waveformOption.audioOption.playbackState = specifyState;
                specifyState !== true
                    ? waveformOption.peaks.player.pause()
                    : waveformOption.peaks.player.play();
            } else {
                // 改变音频播放状态
                waveformOption.audioOption.playbackState === true
                    ? waveformOption.peaks.player.pause()
                    : waveformOption.peaks.player.play();

                // 音频状态取反
                waveformOption.audioOption.playbackState = !waveformOption.audioOption.playbackState;
            }
        },
        /**
         * audioSeek - 音频跳转
         *
         * @function
         * @param { number } targetTime - 指定跳转时间, 单位: 秒
         * */
        audioSeek: function (targetTime) {
            audioStore.state.waveformOption.peaks.player.seek(targetTime);
        },
        /**
         * audioPlaySegment - 播放指定记录片段音频
         *
         * @function
         * @param { Segment } targetSegment - 指定片段
         * @param { boolean } [isLoop = false] - 是否循环播放片段音频
         * */
        audioPlaySegment: function (targetSegment, isLoop = false) {
            audioStore.state.waveformOption.peaks.player.playSegment(targetSegment, isLoop).then(() => {
                // 这里是在开始执行播放时就会触发, 并且循环不会重复触发
            }).catch(() => {});
        }
    },
    // 响应式对象模块
    module: {

    }
}

export default audioStore;

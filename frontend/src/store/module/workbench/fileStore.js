import { h, reactive, ref } from 'vue';
import JSZip from 'jszip';
import { ElMessage, ElMessageBox } from "element-plus";
import workbenchLib from "@/libs/workbench/index.js";

import workbenchStore from "@/store/module/workbench/workbenchStore.js";
import ExportMessage from "@/pages/workbench/components/ExportMessage.vue";
import { deepCopy } from "@/libs/commonFunc/main";
import subtitleStore from "@/store/module/workbench/subtitleStore";

/**
 * fileStore - 全局文件上传 store 对象
 *
 * @const
 * @export
 * @type Object
 * @example { boolean } fileStore.debug - 是否为调试模式
 * @example { Object } fileStore.state - 响应式对象集合对象
 * @example { Object } fileStore.getters - 响应式对象 get 方法集合对象
 * @example { Object } fileStore.setters - 响应式对象 set 方法集合对象
 * @example { Object } fileStore.module - 响应式对象模块
 * */
export const fileStore = {
    // 是否允许调试
    debug: false,
    // 响应式对象集合对象
    state: reactive({
        /**
         * inputBufferObj - 文件上传容器虚拟对象
         * */
        inputBufferObj: ref({
            dom: null,
            eventHandler: {
                change: function (event) {
                    const targetFileOption = fileStore.getters.getTargetFileOption();

                    // 执行文件校验检测函数
                    targetFileOption.beforeRead(event.target.files).then(() => {});
                }
            }
        }),
        /**
         * fileGroupOption - 文件群组配置对象
         * */
        fileGroupOption: {
            // 工程文件群组
            project: {
                label: '工程文件'
            },
            // 字幕工程群组
            subtitle: {
                label: '字幕文件'
            }
        },
        /**
         * fileOption - 文件配置对象
         * */
        fileOption: {
            // mbt project 工程文件
            projectFile: {
                suffixName: 'mbtproject',
                fileGroupKey: 'project',
                label: 'MBT 工程文件',
                multiple: true,
                beforeRead: async function (fileContentArr) {
                    fileStore.getters.getDataFromJSONFile(fileContentArr).then((fileObj) => {
                        // todo 校验相关逻辑
                        const checkResult = true;
                        if (checkResult) {
                            // 解析真实数据
                            const fileDataObj = fileStore.getters.getCoreDataFromParseMBTFile(fileObj);

                            const targetFileOption = fileStore.getters.getTargetFileOption();
                            targetFileOption.read(fileDataObj);
                        } else {
                            ElMessage.error('文件完整性遭破坏!');
                        }
                    });
                },
                read: function (fileDataObj) {
                    // 获取去重后的工程数组
                    const projectArr = [];
                    const projectSet = new Set();
                    Object.keys(fileDataObj).forEach((fileDataKey) => {
                        const targetProjectArr = fileDataObj[fileDataKey].projectArr;

                        targetProjectArr.forEach((targetProjectItem) => {
                            if (!projectSet.has(targetProjectItem.key)) {
                                projectSet.add(targetProjectItem.key);
                                projectArr.push(targetProjectItem);
                            } else {
                                ElMessage.warning(`检测到上传工程数据中包含重名工程: ${ targetProjectItem.key }, 仅读取首个工程数据`);
                            }
                        });
                    });

                    // 创建工程
                    workbenchStore.setters.loadTab(projectArr);

                    // 清空 input 数据
                    fileStore.setters.clearLoadBuffer();
                },
                getDownloadData: function () {
                    // 构建下载数据内容
                    const fileContentObj = {
                        // 文件类型
                        type: 'projectFile',
                        // 用于文件完整性校验
                        checkCode: '',
                        // 用户权限 token
                        token: '',
                        // 数据内容
                        data: workbenchStore.state.projectObj
                    };

                    return JSON.stringify(fileContentObj);
                },
            },
            // mbt subtitle 字幕文件
            subtitleFile: {
                suffixName: 'mbtsubtitle',
                fileGroupKey: 'subtitle',
                label: 'MBT 字幕文件',
                multiple: true,
                beforeRead: async function (fileContentArr) {
                    fileStore.getters.getDataFromJSONFile(fileContentArr).then((fileObj) => {
                        // todo 校验相关逻辑
                        const checkResult = true;
                        if (checkResult) {
                            // 解析真实数据
                            const fileDataObj = fileStore.getters.getCoreDataFromParseMBTFile(fileObj);

                            const targetFileOption = fileStore.getters.getTargetFileOption();
                            targetFileOption.read(fileDataObj);
                        } else {
                            ElMessage.error('文件完整性遭破坏!');
                        }
                    });
                },
                read: function (fileDataObj) {
                    // 添加字幕, 注: 导入机制需清空所有字幕对象且仅允许同时读取一个文件
                    Object.keys(fileDataObj).forEach((fileDataKey) => {
                        subtitleStore.setters.addSubtitle(fileDataObj[fileDataKey].contentArr);
                    });
                },
                getDownloadData: function () {
                    // 获取当前激活工程对象
                    const targetProjectItem = workbenchStore.state.projectObj.projectArr.find((projectItem) => {
                        return projectItem.key === workbenchStore.state.projectObj.activeProjectKey;
                    });

                    // 构建字幕对象文件
                    const fileContentObj = {
                        // 文件类型
                        type: 'subtitleFile',
                        // 用于文件完整性校验
                        checkCode: '',
                        // 用户权限 token
                        token: '',
                        // 数据内容
                        data: targetProjectItem.data.editData.subtitle
                    };

                    return JSON.stringify(fileContentObj);
                }
            },
            // ASS 字幕文件
            assFile: {
                suffixName: 'ass',
                fileGroupKey: 'subtitle',
                label: 'ASS 字幕文件',
                multiple: true,
                beforeRead: async function (fileContentArr) {},
                read: function (fileDataObj) {},
                getDownloadData: function () {}
            },
            // SRT 字幕文件
            srtFile: {
                suffixName: 'srt',
                fileGroupKey: 'subtitle',
                label: 'SRT 字幕文件',
                multiple: true,
                beforeRead: async function (fileContentArr) {},
                read: function (fileDataObj) {},
                getDownloadData: function () {}
            },
            // VTT 字幕文件
            vttFile: {
                suffixName: 'vtt',
                fileGroupKey: 'subtitle',
                label: 'VTT 字幕文件',
                multiple: true,
                beforeRead: async function (fileContentArr) {},
                read: function (fileDataObj) {},
                getDownloadData: function () {}
            },
            // STL 字幕文件
            stlFile: {
                suffixName: 'stl',
                fileGroupKey: 'subtitle',
                label: 'STL 字幕文件',
                multiple: true,
                beforeRead: async function (fileContentArr) {},
                read: function (fileDataObj) {},
                getDownloadData: function () {}
            },
            // SBV 字幕文件
            sbvFile: {
                suffixName: 'sbv',
                fileGroupKey: 'subtitle',
                label: 'SBV 字幕文件',
                multiple: true,
                beforeRead: async function (fileContentArr) {},
                read: function (fileDataObj) {},
                getDownloadData: function () {}
            },
            // SUB 字幕文件
            subFile: {
                suffixName: 'sub',
                fileGroupKey: 'subtitle',
                label: 'SUB 字幕文件',
                multiple: true,
                beforeRead: async function (fileContentArr) {},
                read: function (fileDataObj) {},
                getDownloadData: function () {}
            },
            // DFXP 字幕文件
            dfxpFile: {
                suffixName: 'dfxp',
                fileGroupKey: 'subtitle',
                label: 'DFXP 字幕文件',
                multiple: true,
                beforeRead: async function (fileContentArr) {},
                read: function (fileDataObj) {},
                getDownloadData: function () {}
            },
            // TTML 字幕文件
            ttmlFile: {
                suffixName: 'ttml',
                fileGroupKey: 'subtitle',
                label: 'TTML 字幕文件',
                multiple: true,
                beforeRead: async function (fileContentArr) {},
                read: function (fileDataObj) {},
                getDownloadData: function () {}
            },
        },
        /**
         * activeFileOptionKey - 激活的文件配置对象索引
         * */
        activeFileOptionKey: 'projectFile',
        /**
         * exportEditingCache - 导出数据编辑缓存区, 用于缓存需要导出的文件类型, 用于创建各类导出任务
         * */
        exportEditingCache: {
            // 导出文件配置索引数组
            exportFileOptionKeyArr: []
        }
    }),
    // 响应式对象 get 方法集合对象
    getters: {
        /**
         * getDataFromJSONFile - 读取 JSON 文件并获取文件数据
         *
         * @async
         * @function
         * @param { Array<File> } files - 源文件数组
         * @param { Object } [option = {...}] - 配置参数
         * @return { Object } 文件读取数据对象
         * */
        getDataFromJSONFile: async function  (
            files,
            option = {
                // 读取类型, 可选值有：text, arrayBuffer, dataURL
                type: 'text',
                // 解码类型, 若 type = text 时需要指定
                encoding: 'UTF-8'
            }
        ) {

            // 读取文件对象
            const oriFileObj = {};
            await fileStore.getters.getFileReadData(files, option).then((result) => {
                Object.keys(result).forEach((fileName) => {
                    oriFileObj[fileName] = result[fileName];
                });
            });

            // 解析文件, 获取数据对象, 校验码等
            const fileObj = {};
            Object.keys(oriFileObj).forEach((oriFileKey) => {
                fileObj[oriFileKey] = JSON.parse(oriFileObj[oriFileKey]);
            });

            return fileObj;
        },
        /**
         * getFileReadData - 获取文件读取数据
         *
         * @async
         * @function
         * @param { Array<File> } files - 源文件数组
         * @param { Object } [option = {...}] - 配置参数
         * @return { Object } 文件读取数据对象
         * */
        getFileReadData: async function (
            files,
            option = {
                // 读取类型, 可选值有：text, arrayBuffer, dataURL
                type: 'text',
                // 解码类型, 若 type = text 时需要指定
                encoding: 'UTF-8'
            }
        ) {
            const reObj = {};

            // 创建文件解析异步任务
            const promiseArr = [];
            Array.from(files).forEach((fileItem, fileIndex) => {
                promiseArr.push(new Promise((resolve, reject) => {
                    const fileReader = new FileReader();
                    switch (option.type) {
                        case 'text':
                            fileReader.readAsText(files[fileIndex], 'UTF-8');
                            fileReader.onload = function (event) {
                                resolve({
                                    key: fileItem.name,
                                    value: event.target.result
                                });
                            }
                            break;
                        case 'arrayBuffer':
                            fileReader.readAsArrayBuffer(files[fileIndex]);
                            fileReader.onload = function (event) {
                                resolve({
                                    key: fileItem.name,
                                    value: event.target.result
                                });
                            }
                            break;
                        case 'dataURL':
                            fileReader.readAsDataURL(files[fileIndex]);
                            fileReader.onload = function (event) {
                                resolve({
                                    key: fileItem.name,
                                    value: event.target.result
                                });
                            }
                            break;
                    }
                }));
            });

            // 格式化返回文件解析数据
            await Promise.all(promiseArr).then((resolveArr) => {
                resolveArr.forEach((resolveItem) => {
                    reObj[resolveItem.key] = resolveItem.value;
                });
            });

            return reObj;
        },
        /**
         * getFileOption - 获取指定的文件配置对象
         *
         * @function
         * @param { string } [fileOptionKey = fileStore.state.activeFileOptionKey] - 指定的文件配置对象索引, 默认读取当前正激活的
         * @return { Object } 文件配置对象
         * */
        getTargetFileOption: function (fileOptionKey = fileStore.state.activeFileOptionKey) {
            return fileStore.state.fileOption[fileOptionKey];
        },
        /**
         * getCoreDataFromParseMBTFile - 解析 MBT 类文件并提取其核心数据
         *
         * @function
         * @param { Object } fileObj - 经解码解析后的 MBT 源文件对象
         * @return { Object }  MBT 源文件核心数据对象
         * */
        getCoreDataFromParseMBTFile: function (fileObj) {
            const fileDataObj = {};
            Object.keys(fileObj).forEach((fileKey) => {
                fileDataObj[fileKey] = fileObj[fileKey].data;
            });

            return fileDataObj;
        }
    },
    // 响应式对象 set 方法集合对象
    setters: {
        /**
         * clearLoadBuffer - 清空 loadBuffer 所有记录的数据
         *
         * @function
         * */
        clearLoadBuffer: function () {
            // 利用 input 状态切换清空 FileList 数据
            fileStore.state.inputBufferObj.dom.type = 'text';
            fileStore.state.inputBufferObj.dom.type = 'file';
        },
        /**
         * changeFileOption - 变换文件配置对象
         * 
         * @function
         * @param { string } optionKey - 配置对象索引
         * */
        changeFileOption: function (optionKey) {
            if (fileStore.state.fileOption[optionKey] !== void 0) {
                fileStore.state.activeFileOptionKey = optionKey;
            } else {
                throw Error(`错误的文件配置对象索引 ${ optionKey }`);
            }
        },
        /**
         * supportDownloadCheck - 是否允许下载检测
         *
         * @function
         * @return { boolean } 当前宿主环境是否允许下载
         * */
        supportDownloadCheck: function () {
            const supportDownload = 'download' in document.createElement('a');
            if (!supportDownload) {
                ElMessage.error('当前浏览器不支持文件下载, 请使用 MBT_Client 客户端或使用新版浏览器(chrome浏览器或Edge 79版本以上浏览器)!');
            }

            return supportDownload;
        },
        /**
         * fileSave - 文件保存通用方法
         *
         * @function
         * @param { string } fileContent - 文件内容
         * @param { string } fileName - 文件名称
         * @param { boolean } haveLocalIndex - 是否存在本地文件映射
         * */
        fileSave: function (
            fileContent,
            fileName,
            haveLocalIndex
        ) {
            const targetFileOption = fileStore.getters.getTargetFileOption();
            if (haveLocalIndex) {
                // todo 检测是否存在本地文件映射, 若存在则直接替换本地文件映射, 否则则走另存为
                ElMessage('若该文件存在实际文件路径映射，则直接覆盖实际文件，否则则走另存为逻辑');
            }

            // 另存为逻辑
            workbenchLib.saveDownload(fileContent, `${ fileName }.${ targetFileOption.suffixName }`);
        },
        /**
         * fileExport - 文件导出通用方法
         *
         * @async
         * @function
         * @param { Array<string> } fileOptionKeyArr - 文件配置索引数组
         * @param { string } fileName - 文件名
         * */
        fileExport: async function (fileOptionKeyArr, fileName) {
            const zip = new JSZip();
            const fileOptionObj = fileStore.state.fileOption;

            // 为每个配置文件创建其对应的导出工作项, 并依据其进行汇总至对应文件夹后打包
            fileOptionKeyArr.forEach((fileOptionKey) => {
                const targetOption = fileOptionObj[fileOptionKey];

                // 构建压缩文件
                zip.file(
                    `${ targetOption.fileGroupKey }/${ fileName }.${ targetOption.suffixName }`,
                    targetOption.getDownloadData()
                );
            });

            zip.generateAsync({ type: 'blob' }).then((blobObj) => {
                workbenchLib.saveDownload(blobObj, `${ fileName }.zip`);
            });
        },
        /**
         * runFileSave - 开始文件保存工作
         *
         * @async
         * @function
         * @param { Array<string> } fileOptionKeyArr - 文件配置索引数组
         * @param { string } fileName - 文件名
         * */
        runFileSave: async function (fileOptionKeyArr, fileName) {
            // 非空检测
            if (fileOptionKeyArr.length === 0) {
                throw Error('保存文件配置索引数组不得为空');
            }

            // 宿主环境是否允许下载检测
            const isSupportDownload = fileStore.setters.supportDownloadCheck();
            if (!isSupportDownload) {
                return;
            }

            // 若 fileOptionKeyArr 元素不唯一则走导出逻辑, 否则走保存逻辑
            if (fileOptionKeyArr.length !== 1) {
                fileStore.setters.fileExport(fileOptionKeyArr, fileName).then(() => {});
            } else {
                // 变更至目标文件配置对象
                fileStore.setters.changeFileOption(fileOptionKeyArr[0]);

                // 获取下载数据
                const downloadData = fileStore.state.fileOption[fileOptionKeyArr[0]].getDownloadData();

                // todo 检测是否存在文件的本地缓存, 若存在则在保存时直接覆盖原文件, 否则走另存为
                const haveLocalIndex = true;

                fileStore.setters.fileSave(downloadData, fileName, haveLocalIndex);
            }
        },
        /**
         * runFileRead - 开始文件读取工作
         *
         * @function
         * @param { string } optionKey - 配置对象索引
         * */
        runFileRead: function (optionKey) {
            // 变更至目标文件配置对象
            fileStore.setters.changeFileOption(optionKey);

            // 触发文件上传弹窗
            fileStore.state.inputBufferObj.dom.click();
        },
        /**
         * clearExportEditingCache - 清空导出缓存数据
         *
         * @function
         * @param { Object } [option = {...}] - 清除内容配置对象
         * */
        clearExportEditingCache: function (option = {
            clearExportFileOptionKeyArr: true
        }
        ) {
            // 清空导出文件配置索引数组
            if (option.clearExportFileOptionKeyArr) {
                fileStore.state.exportEditingCache.exportFileOptionKeyArr.length = 0;
            }
        },
        /**
         * readExportEditingCache - 读取导出缓冲区数据
         *
         * @function
         * @return { Object } 导出缓冲区数据
         * */
        readExportEditingCache: function () {
            // 建立导出缓冲区数据拷贝对象
            const exportEditingCacheCopy = deepCopy(fileStore.state.exportEditingCache);
            // 清空导出缓冲区数据
            fileStore.setters.clearExportEditingCache();

            return exportEditingCacheCopy;
        },
        /**
         * setExportFileOptionKeyArrEditingCache - 设置缓存区导出文件配置索引数组数据
         *
         * @function
         * @param { Array<string> } fileSuffixArr - 导出文件后缀数组
         * @param { Object } [option = {...}] - 配置对象
         * */
        setExportFileOptionKeyArrEditingCache: function (fileSuffixArr, option = {
            isCover: true
        }
        ) {
            // 若需覆盖原内容
            if (option.isCover) {
                fileStore.setters.clearExportEditingCache();
            }

            // 数组去重后将未重复添入缓冲区
            const exportFileSuffixSet = new Set(fileStore.state.exportEditingCache.exportFileOptionKeyArr);
            const deduplicatedFileSuffixArr = fileSuffixArr.filter((fileSuffixItem) => {
                return !exportFileSuffixSet.has(fileSuffixItem);
            });

            fileStore.state.exportEditingCache.exportFileOptionKeyArr.push(...deduplicatedFileSuffixArr);
        },
        /**
         * arouseExportMessageBox - 唤起导出通知弹框
         *
         * @function
         * @param { string } fileName - 文件名
         * */
        arouseExportMessageBox: function (fileName) {
            ElMessageBox({
                message: h(ExportMessage),
                title: '导出',
                showClose: false,
                showCancelButton: true,
                confirmButtonText: '导出',
                cancelButtonText: '取消',
                closeOnPressEscape: false,
                closeOnClickModal: false,
                customClass: 'exportMessageBox', // 对应 class 在该动态组件内部维护
                beforeClose: (action, instance, done) => {
                    // 若为点击取消按钮则清空缓冲区
                    if (action === 'cancel'){
                        fileStore.setters.clearExportEditingCache();
                    }
                    done();
                }
            }).then( async function () {
                // 读取缓冲区
                const exportEditingCache = fileStore.setters.readExportEditingCache();
                await fileStore.setters.runFileSave(exportEditingCache.exportFileOptionKeyArr, fileName);

                // 清空导出缓冲区
                fileStore.setters.clearExportEditingCache();
            }).catch(() => {});
        }
    },
    // 响应式对象模块
    module: {

    }
}

export default fileStore;

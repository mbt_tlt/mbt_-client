import { h, reactive, watch } from 'vue';
import { ElMessage, ElMessageBox } from "element-plus";
import { deepCopy } from "@/libs/commonFunc/main.js";

import workbenchStore from "@/store/module/workbench/workbenchStore.js";
import fileStore from "@/store/module/workbench/fileStore.js";
import audioStore from "@/store/module/workbench/audioStore.js";

import AddSubtitleMessage from "@/pages/workbench/components/AddSubtitleMessage.vue";
import LoadFileMessage from "@/pages/workbench/components/LoadFileMessage.vue";
import UpdateSubtitleTimeMessage from "@/pages/workbench/components/UpdateSubtitleTimeMessage.vue";
import UpdateSubtitleContentMessage from "@/pages/workbench/components/UpdateSubtitleContentMessage.vue";

/**
 * subtitleStore - subtitleStore 对象
 *
 * @const
 * @export
 * @type Object
 * @example { boolean } subtitleStore.debug - 是否为调试模式
 * @example { Object } subtitleStore.state - 响应式对象集合对象
 * @example { Object } subtitleStore.getters - 响应式对象 get 方法集合对象
 * @example { Object } subtitleStore.setters - 响应式对象 set 方法集合对象
 * @example { Object } subtitleStore.module - 响应式对象模块
 * */
export const subtitleStore = {
    // 是否允许调试
    debug: false,
    // 响应式对象集合对象
    state: reactive({
        /**
         * subtitleMappingObj - 字幕映射对象, 映射源 workbenchStore.state.projectObj.projectArr[xxx].data.editData.subtitle
         * */
        subtitleMappingObj: {
            '工程1': {
                // 字幕对象组
                contentArr: [
                    {
                        start: 3.14,
                        end: 5,
                        value: {
                            'zh': '测试歌词1测试歌词1测试歌词1测试歌词1测试歌词1测试歌词1测试歌词1测试歌词1测试歌词1测试歌词1测试歌词1测试歌词1',
                        }
                    },
                    {
                        start: 6.14,
                        end: 12,
                        value: {
                            'zh-CN': '测试歌词2',
                            'zh-HK': '測試歌詞2',
                            'en': 'test lyrics 2',
                            'jp': 'テスト歌詞2'
                        }
                    },
                ]
            }
        },
        /**
         * subtitleEditingCache - 字幕编辑缓存区, 用于缓存以各类字幕对象源数据, 这些源数据将通过特定的转化函数转化为对应的字幕对象数据
         * */
        subtitleEditingCache: {
            // 最终检查请求对象, 用于通知动态生成弹框组件的最终检查相关数据
            finalCheckRequestObj: {
                // 请求状态
                state: 'standby',
                // 用于传递记录检查参数
                param: {}
            },
            // 最终检查响应对象, 用于记录动态生成弹框组件的最终检查相关数据
            finalCheckResponseObj: {
                // 响应状态
                state: 'standby',
                // 检查结果对象
                result: {}
            },
            // 时间对象
            timeObj: {
                start: 0,
                end: 0
            },
            // L10n 翻译对象
            languageObj: {},
            // 导入字幕文件列表
            loadFileArr: []
        }
    }),
    // 响应式对象 get 方法集合对象
    getters: {
        /**
         * getNewSubtitleIndex - 获取新字幕索引
         *
         * @function
         * @param { Object } newSubtitleItem - 新字幕对象
         * @return { number } 新字幕索引
         * */
        getNewSubtitleIndex: function (newSubtitleItem) {
            const targetSubtitleArr = subtitleStore.state.subtitleMappingObj[
                workbenchStore.state.projectObj.activeProjectKey
            ].contentArr;

            let newSubtitleIndex = 0;
            if (targetSubtitleArr.length !== 0) {
                // 搜寻新字幕插入位置索引并插入
                const newSubtitleIsTheFirst = targetSubtitleArr[0].start >= newSubtitleItem.end;
                const newSubtitleIsTheLast = targetSubtitleArr[targetSubtitleArr.length - 1].end <= newSubtitleItem.start;

                if (newSubtitleIsTheFirst) {
                    newSubtitleIndex = 0;
                } else if (newSubtitleIsTheLast) {
                    newSubtitleIndex = targetSubtitleArr.length;
                } else {
                    newSubtitleIndex = targetSubtitleArr.findIndex((subtitleItem) => {
                        return subtitleItem.start > newSubtitleItem.end;
                    });
                }
            }

            return newSubtitleIndex;
        },
        /**
         * getSubtitleConflictReason - 获取字幕对象冲突原因
         *
         * @function
         * @param { Object } conflictDetailData - 冲突详细数据对象
         * @return { string } 冲突原因
         * */
        getSubtitleConflictReason: function (conflictDetailData) {
            return Object.keys(conflictDetailData.detailed).find((typeKey) => {
                return conflictDetailData.detailed[typeKey] === true;
            });
        },
        /**
         * isEmptySubtitleArr - 判断指定字幕对象数组是否为空
         *
         * @function
         * @param { string } subtitleObjKey - 字幕对象索引
         * @return { boolean } 指定字幕对象是否为空
         * */
        isEmptySubtitleArr: function (subtitleObjKey) {
            return subtitleStore.state.subtitleMappingObj[subtitleObjKey].contentArr.length === 0;
        }
    },
    // 响应式对象 set 方法集合对象
    setters: {
        /**
         * addSubtitleMappingObjItem - 创建字幕映射源对象
         *
         * @function
         * @param { string } key - 映射源对象索引
         * @param { Object } value - 映射源字幕对象
         * */
        addSubtitleMappingObjItem: function (key, value) {
            // 首先判断指定索引是否存在
            if (subtitleStore.state.subtitleMappingObj[key] !== void 0) {
                throw Error('指定字幕映射源对象已存在!');
            }
            // 创建
            subtitleStore.state.subtitleMappingObj[key] = value;
        },
        /**
         * deleteSubtitleMappingObjItem - 销毁字幕映射源对象
         *
         * @function
         * @param { string } key - 映射源对象索引
         * */
        deleteSubtitleMappingObjItem: function (key) {
            // 首先判断指定索引是否存在
            if (subtitleStore.state.subtitleMappingObj[key] === void 0) {
                throw Error('指定字幕映射源对象不存在!');
            }
            // 删除
            Reflect.deleteProperty(subtitleStore.state.subtitleMappingObj, key);
        },
        /**
         * clearSubtitleMappingObjItemAllSubtitle - 清空指定字幕映射对象所有的字幕对象
         *
         * @function
         * @param { string } key - 映射源对象索引
         * */
        clearSubtitleMappingObjItemAllSubtitle: function (key) {
            subtitleStore.state.subtitleMappingObj[key].contentArr.length = 0;
        },
        /**
         * judgeTimePeriodRepetition - 判断时间段是否有重复
         *
         * @param { Object } testTimeObj - 验证时间段
         * @param { Object } targetTimeObj - 目标时间段
         * @return { Object } 判断结果对象
         * */
        judgeTimePeriodRepetition: function (testTimeObj, targetTimeObj) {
            const testTimeDuration = testTimeObj.end - testTimeObj.start;
            const targetTimeDuration = targetTimeObj.end - targetTimeObj.start;

            // 左相交
            const isLeftIntersection =
                testTimeObj.start > targetTimeObj.start
                && testTimeObj.start < targetTimeObj.end
                && testTimeObj.end > targetTimeObj.end;

            // 右相交
            const isRightIntersection =
                testTimeObj.start < targetTimeObj.start
                && testTimeObj.end > targetTimeObj.start
                && testTimeObj.end < targetTimeObj.end;

            // 内重合
            const isInternalOverlap =
                (targetTimeObj.start < testTimeObj.end && targetTimeObj.end > testTimeObj.start)
                && testTimeDuration > targetTimeDuration
                && (!isLeftIntersection && !isRightIntersection);

            // 外重合
            const isExternalOverlap =
                (testTimeObj.start < targetTimeObj.end && testTimeObj.end > targetTimeObj.start)
                && testTimeDuration < targetTimeDuration
                && (!isLeftIntersection && !isRightIntersection);

            // 完全重合
            const isConsistent = testTimeObj.start === targetTimeObj.start && testTimeObj.end === targetTimeObj.end;

            // 是否重合
            const isOverlap = isInternalOverlap || isConsistent || isExternalOverlap;

            return {
                value: isLeftIntersection || isOverlap || isRightIntersection,
                detailed: {
                    'isLeftIntersection': isLeftIntersection,
                    'isRightIntersection': isRightIntersection,
                    'isInternalOverlap': isInternalOverlap,
                    'isExternalOverlap': isExternalOverlap,
                    'isConsistent': isConsistent,
                    'isOverlap': isOverlap,
                }
            };
        },
        /**
         * judgeDuplicateSubtitle - 判断是否有重复字幕对象
         *
         * @function
         * @param { Object } testSubtitleItem - 待检测的字幕对象或任何具有 start, end 属性的时间轴对象
         * @param { Array<number> } [ignoreDetectedArr = []] - 忽略检测列表, 内包含忽略检测的字幕对象索引
         * @return { Object } 判断结果对象
         * */
        judgeDuplicateSubtitle: function (testSubtitleItem, ignoreDetectedArr = []) {
            let conflictDetailData = {};
            const targetSubtitleArr = subtitleStore.state.subtitleMappingObj[workbenchStore.state.projectObj.activeProjectKey].contentArr;

            const ignoreDetectedSet = new Set(ignoreDetectedArr);
            const firstConflictIndex = targetSubtitleArr.findIndex((subtitleItem, subtitleItemIndex) => {
                // 若属于 忽略检测字幕, 则直接跳过当前冲突检测
                if (ignoreDetectedSet.has(subtitleItemIndex)) {
                    return false;
                }

                // 冲突检测
                const timePeriodRepetitionResult = workbenchStore.module.subtitleStore.setters.judgeTimePeriodRepetition(subtitleItem, testSubtitleItem);
                conflictDetailData = timePeriodRepetitionResult;

                return timePeriodRepetitionResult.value;
            });

            return {
                // 是否存在冲突
                isConflict: firstConflictIndex !== -1,
                // 首个冲突对象索引
                firstConflictIndex: firstConflictIndex,
                // 冲突详细数据对象
                conflictDetailData: conflictDetailData
            }
        },
        /**
         * addSubtitle - 添加字幕
         *
         * @function
         * @param { Array<Object> } newSubtitleArr - 新字幕对象数组
         * */
        addSubtitle: function (newSubtitleArr) {
            const targetSubtitleArr = subtitleStore.state.subtitleMappingObj[
                workbenchStore.state.projectObj.activeProjectKey
            ].contentArr;

            // 插值添加
            newSubtitleArr.forEach((newSubtitleItem) => {
                const newSubtitleIndex = subtitleStore.getters.getNewSubtitleIndex(newSubtitleItem);
                // 插入 todo 后续这里添加一个跳转到编辑栏添加字幕处的功能
                targetSubtitleArr.splice(newSubtitleIndex, 0, newSubtitleItem);
            });

            ElMessage.success('已成功添加字幕');
        },
        /**
         * deleteSubtitle - 删除字幕
         *
         * @function
         * @param { number } targetSubtitleIndex - 目标字幕对象索引
         * */
        deleteSubtitle: function (targetSubtitleIndex) {
            ElMessageBox.confirm(
                '确定要删除该字幕对象?',
                '确定删除',
                {
                    confirmButtonText: '是',
                    cancelButtonText: '否',
                    type: 'warning',
                }
            ).then(() => {
                const targetSubtitleArr = subtitleStore.state.subtitleMappingObj[workbenchStore.state.projectObj.activeProjectKey].contentArr;
                targetSubtitleArr.splice(targetSubtitleIndex, 1);

                ElMessage({
                    type: 'success',
                    message: '已删除目标字幕对象'
                })
            }).catch(() => {});
        },
        /**
         * addSubtitleLanguage - 添加字幕语言组
         *
         * @function
         * @param { number } subtitleIndex - 指定字幕索引
         * */
        addSubtitleLanguage: function (subtitleIndex) {
            const newSubtitleLanguageKey = 'en-UK';
            const newSubtitleLanguageValue = 'test lyrics 0';

            // 获取指定字幕对象
            const targetSubtitleArr = subtitleStore.state.subtitleMappingObj[workbenchStore.state.projectObj.activeProjectKey].contentArr;
            const targetSubtitle = targetSubtitleArr[subtitleIndex];

            // 添加字幕
            targetSubtitle.value[newSubtitleLanguageKey] = newSubtitleLanguageValue;
        },
        /**
         * deleteSubtitleLanguage - 删除字幕语言组
         * 
         * @function
         * @param { number } subtitleIndex - 指定字幕索引
         * @param { string } languageKey - 指定字幕的语言索引
         * */
        deleteSubtitleLanguage: function (subtitleIndex, languageKey) {
            ElMessageBox.confirm(
                '确定要删除该字幕翻译语言对象?',
                '确定删除',
                {
                    confirmButtonText: '是',
                    cancelButtonText: '否',
                    type: 'warning',
                }
            ).then(() => {
                // 获取指定字幕对象
                const targetSubtitleArr = subtitleStore.state.subtitleMappingObj[workbenchStore.state.projectObj.activeProjectKey].contentArr;
                const targetSubtitle = targetSubtitleArr[subtitleIndex];

                Reflect.deleteProperty(targetSubtitle.value, languageKey);
                ElMessage({
                    type: 'success',
                    message: '已删除目标字幕翻译语言对象'
                })
            }).catch(() => {});
        },
        /**
         * clearSubtitleEditingCache - 清理字幕缓存区数据
         *
         * @function
         * @param { Object } [option = {...}] - 清除内容索引
         * */
        clearSubtitleEditingCache: function (option = {
            clearFinalCheckRequest: false,
            clearFinalCheckResponse: false,
            clearTimeObj: true,
            clearLanguageObj: true,
            clearLoadFileArr: true
        }
        ) {
            // 重置最终检查请求对象数据
            if (option.clearFinalCheckRequest) {
                subtitleStore.state.subtitleEditingCache.finalCheckRequestObj.state = 'standby';
                for (let paramKey in subtitleStore.state.subtitleEditingCache.finalCheckRequestObj.param) {
                    Reflect.deleteProperty(subtitleStore.state.subtitleEditingCache.finalCheckRequestObj.param, paramKey);
                }
            }

            // 重置最终检查响应对象数据
            if (option.clearFinalCheckResponse) {
                subtitleStore.state.subtitleEditingCache.finalCheckResponseObj.state = 'standby';
                for (let resultKey in subtitleStore.state.subtitleEditingCache.finalCheckResponseObj.result) {
                    Reflect.deleteProperty(subtitleStore.state.subtitleEditingCache.finalCheckResponseObj.result, resultKey);
                }
            }

            // 初始化字幕编辑区时间对象
            if (option.clearTimeObj) {
                subtitleStore.state.subtitleEditingCache.timeObj.start = 0;
                subtitleStore.state.subtitleEditingCache.timeObj.end = 0;
            }

            // 初始化字幕编辑区 L10n 翻译对象
            if (option.clearLanguageObj) {
                for (let languageKey in subtitleStore.state.subtitleEditingCache.languageObj) {
                    Reflect.deleteProperty(subtitleStore.state.subtitleEditingCache.languageObj, languageKey);
                }
            }

            // 初始化导入字幕文件列表
            if (option.clearLoadFileArr) {
                subtitleStore.state.subtitleEditingCache.loadFileArr.length = 0;
            }
        },
        /**
         * readSubtitleEditingCache - 读取字幕缓存区数据
         *
         * @function
         * @return { Object } 读取字幕缓存区数据
         * */
        readSubtitleEditingCache: function () {
            // 建立字幕缓存区数据拷贝对象
            const subtitleEditingCacheCopy = deepCopy(subtitleStore.state.subtitleEditingCache);
            // 清理字幕缓存区数据
            subtitleStore.setters.clearSubtitleEditingCache();

            return subtitleEditingCacheCopy;
        },
        /**
         * setSubtitleFinalCheckRequestObjEditingCache - 设置字幕缓存区最终检查请求数据
         *
         * @function
         * @param { string } state - 字幕缓存区最终检查请求状态
         * @param { Object } param - 字幕缓存区最终检查请求数据
         * */
        setSubtitleFinalCheckRequestObjEditingCache: function (state, param) {
            subtitleStore.state.subtitleEditingCache.finalCheckRequestObj.state = state;
            subtitleStore.state.subtitleEditingCache.finalCheckRequestObj.param = param;
        },
        /**
         * setSubtitleFinalCheckResponseObjEditingCache - 设置字幕缓存区最终检查响应数据
         *
         * @function
         * @param { string } state - 字幕缓存区最终检查响应状态
         * @param { Object } result - 字幕缓存区最终检查响应数据
         * */
        setSubtitleFinalCheckResponseObjEditingCache: function (state, result) {
            subtitleStore.state.subtitleEditingCache.finalCheckResponseObj.state = state;
            subtitleStore.state.subtitleEditingCache.finalCheckResponseObj.result = result;
        },
        /**
         * setSubtitleTimeEditingCache - 设置字幕缓存区时间数据
         *
         * @function
         * @param { string } key - 字幕缓存区时间数据索引
         * @param { number } value - 字幕缓存区时间数据索引对应数据
         * */
        setSubtitleTimeEditingCache: function (key, value) {
            subtitleStore.state.subtitleEditingCache.timeObj[key] = value;
        },
        /**
         * setSubtitleLanguageEditingCache - 设置字幕缓存区 L10n 翻译对象数据
         *
         * @function
         * @param { Object } obj - 字幕缓存区时间 L10n 翻译对象数据索引对应数据
         * */
        setSubtitleLanguageEditingCache: function (obj) {
            const languageCacheObj = subtitleStore.state.subtitleEditingCache.languageObj;

            // 清除多余值
            Object.keys(languageCacheObj).forEach((languageKey) => {
                if (obj[languageKey] === void 0) {
                    Reflect.deleteProperty(languageCacheObj, languageKey);
                }
            });

            // 重新赋值并创建新值
            Object.keys(obj).forEach((languageKey) => {
                languageCacheObj[languageKey] = obj[languageKey];
            });
        },
        /**
         * setSubtitleLoadFileArrEditingCache - 设置字幕缓存区 导入字幕文件列表
         *
         * @function
         * @param { Array<File> } fileArr - 导入字幕文件列表
         * @param { Object } [option = {...}] - 配置对象
         * */
        setSubtitleLoadFileArrEditingCache: function (fileArr, option = {
            isCover: true
        }
        ) {
            const loadFileArrEditingCache = subtitleStore.state.subtitleEditingCache.loadFileArr;

            // 若需覆盖原内容
            if (option.isCover) {
                loadFileArrEditingCache.length = 0;
            }

            // 数组去重后将未重复添入缓冲区
            const editingLoadFileNameArr = [];
            loadFileArrEditingCache.forEach((loadFileItem) => {
                editingLoadFileNameArr.push(loadFileItem.name);
            });
            const loadFileNameSet = new Set(editingLoadFileNameArr);

            const deduplicatedFileArr = fileArr.filter((fileItem) => {
                return !loadFileNameSet.has(fileItem.name);
            });

            loadFileArrEditingCache.push(...deduplicatedFileArr);
        },
        /**
         * arouseAddSubtitleMessageBox - 唤起添加字幕弹框
         *
         * @function
         * */
        arouseAddSubtitleMessageBox: function () {
            ElMessageBox({
                message: h(AddSubtitleMessage),
                title: '新建字幕',
                showClose: false,
                showCancelButton: true,
                confirmButtonText: '新建',
                cancelButtonText: '取消',
                closeOnPressEscape: false,
                closeOnClickModal: false,
                customClass: 'addSubtitleMessageBox', // 对应 class 在该动态组件内部维护
                beforeClose: (action, instance, done) => {
                    // 若为点击取消按钮, 则清空字幕编辑缓存区后关闭弹窗并提前结束该守备函数
                    if (action === 'cancel'){
                        subtitleStore.setters.clearSubtitleEditingCache();
                        done();
                        return ;
                    }

                    // 更新字幕编辑缓存区请求对象
                    subtitleStore.setters.setSubtitleFinalCheckRequestObjEditingCache('accept', {
                        duration: audioStore.getters.getAudioDataInformation().duration
                    });

                    // 监听字幕编辑缓存区请求对象
                    watch(() => subtitleStore.state.subtitleEditingCache.finalCheckResponseObj, (newVal) => {
                        if (newVal.state === 'accept') {
                            // 若完成验证则允许关闭通知框, 没有就重置最终检查响应对象数据
                            newVal.result.value === true ? done() : subtitleStore.setters.clearSubtitleEditingCache({
                                clearFinalCheckRequest: true,
                                clearFinalCheckResponse: true,
                                clearTimeObj: false,
                                clearLanguageObj: false,
                                clearLoadFileArr: false
                            });
                        }
                    }, { deep: true });
                }
            }).then(() => {
                // 读取并格式化编辑对象并以此创建新的字幕对象
                const subtitleEditingCacheItem = subtitleStore.setters.readSubtitleEditingCache();

                // 添加字幕
                subtitleStore.setters.addSubtitle([{
                    start: subtitleEditingCacheItem.timeObj.start,
                    end: subtitleEditingCacheItem.timeObj.end,
                    value: subtitleEditingCacheItem.languageObj
                }]);

                // 清空字幕编辑缓存区
                subtitleStore.setters.clearSubtitleEditingCache({
                    clearFinalCheckRequest: true,
                    clearFinalCheckResponse: true,
                    clearTimeObj: true,
                    clearLanguageObj: true,
                    clearLoadFileArr: true
                });
            }).catch(() => {});
        },
        /**
         * arouseLoadSubtitleMessageBox - 唤起导入字幕弹窗
         *
         * @function
         * */
        arouseLoadSubtitleMessageBox: function () {
            // 获取 fileGroupKey 为 subtitle 的文件配置列表
            const subtitleFileOptionArr = Object.values(fileStore.state.fileOption).filter((fileOptionItem) => {
                return fileOptionItem.fileGroupKey === 'subtitle';
            });
            const suffixNameArr = [];
            subtitleFileOptionArr.forEach((subtitleFileOption) => {
                suffixNameArr.push(subtitleFileOption.suffixName);
            });

            ElMessageBox({
                message: h(
                    LoadFileMessage,
                    {
                        suffixNameArr: suffixNameArr
                    }
                ),
                title: '导入字幕文件',
                showClose: false,
                showCancelButton: true,
                confirmButtonText: '导入',
                cancelButtonText: '取消',
                closeOnPressEscape: false,
                closeOnClickModal: false,
                customClass: 'LoadFileMessageBox', // 对应 class 在该动态组件内部维护
                beforeClose: (action, instance, done) => {
                    // 若为点击取消按钮则清空缓冲区后直接关闭
                    if (action === 'cancel'){
                        subtitleStore.setters.clearSubtitleEditingCache();
                        done();
                        return;
                    }

                    // 若没有上传文件则弹窗提示并且拦截关闭窗口
                    if (subtitleStore.state.subtitleEditingCache.loadFileArr.length === 0) {
                        ElMessage.warning({
                            message: `请选择指定文件后再点击导入`,
                            grouping: true
                        });
                    } else {
                        done();
                    }
                }
            }).then( async function () {
                // 读取缓冲区
                const subtitleEditingCacheCopy = subtitleStore.setters.readSubtitleEditingCache();

                // 将 loadFileArr 进行分类, 将相同后缀的文件分组
                const loadFileObj = {};
                subtitleEditingCacheCopy.loadFileArr.forEach((fileItem) => {
                    const fileNameScrapArr = fileItem.name.split('.');
                    const fileSuffixName = fileNameScrapArr[fileNameScrapArr.length - 1];

                    // 根据 fileSuffixName 寻找到指定 targetFileOptionKey 对象
                    const fileOptionKey = Object.keys(fileStore.state.fileOption).find((fileOptionKey) => {
                        return fileStore.state.fileOption[fileOptionKey].suffixName === fileSuffixName;
                    });

                    if (!loadFileObj.hasOwnProperty(fileOptionKey)) {
                        loadFileObj[fileOptionKey] = [];
                    }

                    loadFileObj[fileOptionKey].push(fileItem);
                });

                // 创建读取任务对象组
                const promiseArr = [];
                for (let fileOptionKey in loadFileObj) {
                    const readFileTask = new Promise((resolve, reject) => {
                        // 变更至目标文件配置对象后执行读取任务
                        fileStore.setters.changeFileOption(fileOptionKey);
                        resolve(fileStore.state.fileOption[fileOptionKey].beforeRead(loadFileObj[fileOptionKey]));
                    });

                    promiseArr.push(readFileTask);
                }

                // 执行指定的读取任务
                await Promise.all(promiseArr).then(() => {});

                // 清空导出缓冲区
                subtitleStore.setters.clearSubtitleEditingCache();
            }).catch(() => {});
        },
        /**
         * arouseUpdateSubtitleTimeMessageBox - 唤起修改字幕时间弹框
         *
         * @function
         * @param { number } subtitleIndex - 字幕对象索引
         * */
        arouseUpdateSubtitleTimeMessageBox: function (subtitleIndex) {
            ElMessageBox({
                message: h(
                    UpdateSubtitleTimeMessage,
                    {
                        subtitleIndex: subtitleIndex
                    }
                ),
                title: '修改字幕时间',
                showClose: false,
                showCancelButton: true,
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                closeOnPressEscape: false,
                closeOnClickModal: false,
                customClass: 'updateSubtitleTimeMessageBox', // 对应 class 在该动态组件内部维护
                beforeClose: (action, instance, done) => {
                    // 若为点击取消按钮则清空缓冲区后直接关闭
                    if (action === 'cancel'){
                        subtitleStore.setters.clearSubtitleEditingCache();
                        done();
                        return;
                    }

                    // 更新字幕编辑缓存区请求对象
                    subtitleStore.setters.setSubtitleFinalCheckRequestObjEditingCache('accept', {
                        duration: audioStore.getters.getAudioDataInformation().duration
                    });

                    // 监听字幕编辑缓存区请求对象
                    watch(() => subtitleStore.state.subtitleEditingCache.finalCheckResponseObj, (newVal) => {
                        if (newVal.state === 'accept') {
                            // 若完成验证则允许关闭通知框, 没有就重置最终检查响应对象数据
                            newVal.result.value === true ? done() : subtitleStore.setters.clearSubtitleEditingCache({
                                clearFinalCheckRequest: true,
                                clearFinalCheckResponse: true,
                                clearTimeObj: false,
                                clearLanguageObj: false,
                                clearLoadFileArr: false
                            });
                        }
                    }, { deep: true });
                }
            }).then( async function () {
                // 读取缓冲区
                const subtitleEditingCacheCopy = subtitleStore.setters.readSubtitleEditingCache();

                // 修改指定的字幕时间数据
                const activeProject = workbenchStore.state.projectObj.projectArr.find((projectItem) => {
                    return projectItem.key === workbenchStore.state.projectObj.activeProjectKey;
                });
                const subtitleArr = activeProject.data.editData.subtitle.contentArr;

                subtitleArr[subtitleIndex].start = subtitleEditingCacheCopy.timeObj.start;
                subtitleArr[subtitleIndex].end = subtitleEditingCacheCopy.timeObj.end;

                // 重排字幕列表顺序【将修改的字幕对象提取出后重新依据时间顺序插入】
                const targetSubtitleItem = subtitleArr.splice(subtitleIndex, 1)[0];
                const newSubtitleIndex = subtitleStore.getters.getNewSubtitleIndex(targetSubtitleItem);
                subtitleArr.splice(newSubtitleIndex, 0, targetSubtitleItem);

                // todo 后续这里添加一个跳转到编辑栏添加字幕处的功能

                // 清空导出缓冲区
                subtitleStore.setters.clearSubtitleEditingCache({
                    clearFinalCheckRequest: true,
                    clearFinalCheckResponse: true,
                    clearTimeObj: true,
                    clearLanguageObj: true,
                    clearLoadFileArr: true
                });
            }).catch(() => {});
        },
        /**
         * arouseUpdateSubtitleContentMessageBox - 唤起修改字幕内容弹框
         *
         * @function
         * @param { number } subtitleIndex - 字幕对象索引
         * */
        arouseUpdateSubtitleContentMessageBox: function (subtitleIndex) {
            ElMessageBox({
                message: h(
                    UpdateSubtitleContentMessage,
                    {
                        subtitleIndex: subtitleIndex
                    }
                ),
                title: '修改字幕内容',
                showClose: false,
                showCancelButton: true,
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                closeOnPressEscape: false,
                closeOnClickModal: false,
                customClass: 'updateSubtitleContentMessageBox', // 对应 class 在该动态组件内部维护
                beforeClose: (action, instance, done) => {
                    // 若为点击取消按钮则清空缓冲区后直接关闭
                    if (action === 'cancel'){
                        subtitleStore.setters.clearSubtitleEditingCache();
                        done();
                        return;
                    }

                    // 更新字幕编辑缓存区请求对象
                    subtitleStore.setters.setSubtitleFinalCheckRequestObjEditingCache('accept', {});

                    // 监听字幕编辑缓存区请求对象
                    watch(() => subtitleStore.state.subtitleEditingCache.finalCheckResponseObj, (newVal) => {
                        if (newVal.state === 'accept') {
                            // 若完成验证则允许关闭通知框, 没有就重置最终检查响应对象数据
                            newVal.result.value === true ? done() : subtitleStore.setters.clearSubtitleEditingCache({
                                clearFinalCheckRequest: true,
                                clearFinalCheckResponse: true,
                                clearTimeObj: false,
                                clearLanguageObj: false,
                                clearLoadFileArr: false
                            });
                        }
                    }, { deep: true });
                }
            }).then( async function () {
                // 读取缓冲区
                const subtitleEditingCacheCopy = subtitleStore.setters.readSubtitleEditingCache();

                // 修改指定的字幕时间数据
                const activeProject = workbenchStore.state.projectObj.projectArr.find((projectItem) => {
                    return projectItem.key === workbenchStore.state.projectObj.activeProjectKey;
                });
                const subtitleArr = activeProject.data.editData.subtitle.contentArr;

                // 遍历赋值【删除原值后添加缓存值】
                Object.keys(subtitleArr[subtitleIndex].value).forEach((languageKey) => {
                    Reflect.deleteProperty(subtitleArr[subtitleIndex].value, languageKey);
                });
                Object.keys(subtitleEditingCacheCopy.languageObj).forEach((languageKey) => {
                    subtitleArr[subtitleIndex].value[languageKey] = subtitleEditingCacheCopy.languageObj[languageKey];
                });

                // todo 后续这里添加一个跳转到编辑栏添加字幕处的功能

                // 清空导出缓冲区
                subtitleStore.setters.clearSubtitleEditingCache({
                    clearFinalCheckRequest: true,
                    clearFinalCheckResponse: true,
                    clearTimeObj: true,
                    clearLanguageObj: true,
                    clearLoadFileArr: true
                });
            }).catch(() => {});
        }
    },
    // 响应式对象模块
    module: {

    }
}

export default subtitleStore;

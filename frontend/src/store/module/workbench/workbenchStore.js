import { markRaw, reactive, ref } from 'vue';
import { i18n } from "@/libs/commonFunc/i18n";
import { ElMessage, ElMessageBox } from "element-plus";

import audioStore from "@/store/module/workbench/audioStore.js";
import subtitleStore from "@/store/module/workbench/subtitleStore.js";
import fileStore from "@/store/module/workbench/fileStore.js";

import SubtitleEditBar from "@/pages/workbench/subtitleEditBar/SubtitleEditBar.vue";
import TestEditBar from "@/pages/workbench/testEditBar/TestEditBar.vue";
import LyricShowBar from "@/pages/workbench/lyricShowBar/LyricShowBar.vue";

/**
 * workbenchStore - 工作台 store 对象
 *
 * @const
 * @export
 * @type Object
 * @example { boolean } workbenchStore.debug - 是否为调试模式
 * @example { Object } workbenchStore.state - 响应式对象集合对象
 * @example { Object } workbenchStore.getters - 响应式对象 get 方法集合对象
 * @example { Object } workbenchStore.setters - 响应式对象 set 方法集合对象
 * @example { Object } workbenchStore.module - 响应式对象模块
 * */
export const workbenchStore = {
    // 是否允许调试
    debug: false,
    // 响应式对象集合对象
    state: reactive({
        /**
         * projectObj - 工程虚拟对象
         * */
        projectObj: {
            activeProjectKey: '',
            projectArr:  [
                {
                    key: '工程1',
                    data: {
                        // audio 相关数据
                        audioData: audioStore.state.audioMappingObj['工程1'],
                        // peaks 相关数据
                        peaksData: {},
                        // edit 相关数据
                        editData: {
                            // 字幕相关数据
                            subtitle: subtitleStore.state.subtitleMappingObj['工程1'],
                        }
                    }
                }
            ]
        },
        /**
         * topBarMenuOption - 顶部菜单栏配置对象
         * */
        topBarMenuOption: ref({
            dom: null,
            content: [
                // 工具菜单栏
                {
                    key: 'fileMenu',
                    type: 'menu',
                    class: '',
                    title: '文件',
                    content: [
                        // 新建
                        {
                            key: 'fileMenu-newProject',
                            type: 'menuItem',
                            eventHandler: {
                                click: function () {
                                    workbenchStore.setters.addTab();
                                }
                            },
                            content: '新建'
                        },
                        // 打开
                        {
                            key: 'fileMenu-openFile',
                            type: 'menuItem',
                            eventHandler: {
                                click: function (event) {
                                    workbenchStore.module.fileStore.setters.runFileRead('projectFile');
                                }
                            },
                            content: '打开'
                        },
                        // 保存
                        {
                            key: 'fileMenu-save',
                            type: 'menuItem',
                            eventHandler: {
                                click: function () {
                                    // 保存文件
                                    fileStore.setters.runFileSave(
                                        ['projectFile'],
                                        workbenchStore.state.projectObj.activeProjectKey
                                    ).then(() => {});
                                }
                            },
                            content: '保存'
                        },
                        // 导出
                        {
                            key: 'fileMenu-export',
                            type: 'menuItem',
                            eventHandler: {
                                click: function () {
                                    // 唤起导出弹框
                                    workbenchStore.module.fileStore.setters.arouseExportMessageBox(workbenchStore.state.projectObj.activeProjectKey);
                                }
                            },
                            content: '导出'
                        },
                        // 重置
                        {
                            key: 'fileMenu-reset',
                            type: 'menuItem',
                            eventHandler: {
                                click: function () {
                                    ElMessage('之后这里弹出个确认框, 若确定则重新初始化整个工作台')
                                }
                            },
                            content: '重置'
                        },
                    ]
                },
                // 帮助
                {
                    key: 'helpItem',
                    type: 'default',
                    iconStyle: 'fa-info-circle',
                    cursor: 'default',
                    content: '帮助',
                    eventHandler: {
                        click: function () {
                            ElMessage('之后这里弹出帮助教程')
                        }
                    }
                },
            ]
        }),
        /**
         * leftSidebarOption - 左侧工具栏配置对象
         * */
        leftSidebarOption: ref({
            dom: null,
            // 视图虚拟列表
            contentList: [
                {
                    // 唯一性索引
                    key: 'subtitleViewNav',
                    // 名称简写
                    // abbreviatedName: i18n('viewStore-userSettingView-abbreviatedName'),
                    abbreviatedName: '幕',
                    // 定位
                    position: 'top',
                    // fontawesome 图标样式
                    iconStyle: 'fa-terminal',
                    // 图片路径
                    imgSrc: '',
                    // 提示框内容
                    tooltip: '音频字幕制作',
                    // 关联工作区视图对象索引
                    editBarViewKey: 'subtitleView',
                    // 关联展示区视图对象索引
                    showBarViewKey: 'lyricView',
                    // 关联工作区视图组件对象
                    editBarViewComponent: markRaw(SubtitleEditBar),
                    // 关联展示区视图组件对象
                    showBarViewComponent: markRaw(LyricShowBar)
                },
                {
                    key: 'testViewNav',
                    abbreviatedName: '测',
                    position: 'top',
                    iconStyle: '',
                    imgSrc: '',
                    tooltip: '测试用组件',
                    editBarViewKey: 'testView',
                    showBarViewKey: 'lyricView',
                    editBarViewComponent: markRaw(TestEditBar),
                    showBarViewComponent: markRaw(LyricShowBar)
                }
            ],
            data: {}
        }),
        /**
         * editBarOption - 编辑区配置对象
         * */
        editBarOption: ref({
            dom: null,
            loading: false,
            data: {
                activeViewKey: 'subtitleView',
                viewsObj: {
                    // 字幕编辑视图
                    subtitleView: {
                        toolArr: [
                            {
                                // 唯一性索引
                                key: 'addSubtitle',
                                // 名称简写
                                // abbreviatedName: i18n('viewStore-userSettingView-abbreviatedName'),
                                abbreviatedName: '+',
                                // 定位
                                position: 'center',
                                // fontawesome 图标样式
                                iconStyle: '',
                                // iconStyle: 'fa-map-marker',
                                // 图片路径
                                imgSrc: '',
                                // 提示框内容
                                tooltip: '添加字幕',
                                // 触发时执行的指令
                                executeInstruction: function () {
                                    workbenchStore.module.subtitleStore.setters.arouseAddSubtitleMessageBox();
                                }
                            },
                            {
                                key: 'importSubtitle',
                                // abbreviatedName: i18n('viewStore-userSettingView-abbreviatedName'),
                                abbreviatedName: '导',
                                position: 'center',
                                iconStyle: 'fa-download',
                                imgSrc: '',
                                tooltip: '导入字幕',
                                executeInstruction: function () {
                                    const isTargetProjectSubtitleEmpty =
                                        workbenchStore.module.subtitleStore.getters.isEmptySubtitleArr(
                                            workbenchStore.state.projectObj.activeProjectKey
                                        );

                                    // 判断当前编辑区是否为空, 若不为空则弹窗询问是否需要清空编辑区字幕
                                    if (isTargetProjectSubtitleEmpty) {
                                        workbenchStore.module.subtitleStore.setters.arouseLoadSubtitleMessageBox();
                                    } else {
                                        ElMessageBox({
                                            message: '确认导入字幕? 这将会清空当前工程的所有字幕【无法恢复】',
                                            title: '当前工程字幕列表不为空',
                                            confirmButtonText: '确认',
                                            cancelButtonText: '取消',
                                            showCancelButton: true,
                                            type: 'warning',
                                        }).then(() => {
                                            workbenchStore.module.subtitleStore.setters.clearSubtitleMappingObjItemAllSubtitle(workbenchStore.state.projectObj.activeProjectKey);
                                            workbenchStore.module.subtitleStore.setters.arouseLoadSubtitleMessageBox();
                                        }).catch(() => {});
                                    }
                                }
                            },
                            {
                                key: 'codeSubtitle',
                                // abbreviatedName: i18n('viewStore-userSettingView-abbreviatedName'),
                                abbreviatedName: '码',
                                position: 'center',
                                iconStyle: 'fa-code',
                                imgSrc: '',
                                tooltip: '以代码形式编辑字幕',
                                executeInstruction: function () {
                                    ElMessage('以代码形式编辑字幕')
                                }
                            },
                        ]
                    },
                    // 测试用例视图
                    testView: {
                        toolArr: []
                    }
                }
            }
        }),
        /**
         * showBarOption - 效果展示区区配置对象
         * */
        showBarOption: ref({
            dom: null,
            loading: false,
            data: {
                activeViewKey: 'lyricView',
                viewsObj: {
                    // 歌词展示视图
                    lyricView: {
                        toolArr: [
                            {
                                key: 'audioSwitch',
                                abbreviatedName: '',
                                position: 'center',
                                iconStyle: 'fa-play',
                                imgSrc: '',
                                tooltip: '播放开关',
                                executeInstruction: function () {
                                    // 切换音频播放状态
                                    audioStore.setters.changeAudioPlaybackState();
                                }
                            },
                            {
                                key: 'audioRepeat',
                                abbreviatedName: '',
                                position: 'center',
                                iconStyle: 'fa-rotate-left',
                                imgSrc: '',
                                tooltip: '音频重置',
                                executeInstruction: function () {
                                    // 音频跳转到起始处并提示
                                    audioStore.setters.audioSeek(0);
                                    // 暂停音频播放
                                    audioStore.setters.changeAudioPlaybackState(false);

                                    ElMessage.info('音频已重置');
                                }
                            },
                        ]
                    }
                }
            }
        }),
    }),
    // 响应式对象 get 方法集合对象
    getters: {

    },
    // 响应式对象 set 方法集合对象
    setters: {
        /**
         * projectConstruct - 构建工程
         *
         * @function
         * @param { Object } oriProjectItem - 工程源数据对象
         * */
        projectConstruct: function (oriProjectItem) {
            const tabArr = workbenchStore.state.projectObj.projectArr;

            // 创建工程所属音频虚拟对象, 并重构工程对象与其音频对象的映射关系
            workbenchStore.module.audioStore.setters.addAudioMappingObjItem(
                oriProjectItem.key,
                {
                    src: '/public/audio/mothy - 第1幕2曲　「Ma」計画.mp3',
                    audioArrayBuffer: null,
                    decodeAudio: null
                }
            );
            oriProjectItem.data.audioData = workbenchStore.module.audioStore.state.audioMappingObj[oriProjectItem.key];

            // 创建工程所属字幕虚拟对象, 并重构工程对象与其字幕映射对象的关系
            workbenchStore.module.subtitleStore.setters.addSubtitleMappingObjItem(
                oriProjectItem.key,
                JSON.parse(JSON.stringify(oriProjectItem.data.editData.subtitle))
            );
            oriProjectItem.data.editData.subtitle
                = workbenchStore.module.subtitleStore.state.subtitleMappingObj[oriProjectItem.key];

            // 添加工程
            tabArr.push(oriProjectItem);
            // 切换 peaks 关联的音频源
            workbenchStore.module.audioStore.setters.changePeaksSource(oriProjectItem.data.audioData.src);
            // 切换激活标签页为新标签页
            workbenchStore.setters.changeActiveTab(tabArr[tabArr.length - 1].key);
        },
        /**
         * projectDestroy - 销毁工程
         *
         * @function
         * @param { string } projectKey - 工程源数据对象索引
         * */
        projectDestroy: function (projectKey) {
            const tabArr = workbenchStore.state.projectObj.projectArr;

            const oriRepeatTabIndex = tabArr.findIndex((tabItem) => {
                return tabItem.key === projectKey;
            });
            // 删除所属的字幕映射对象
            workbenchStore.module.subtitleStore.setters.deleteSubtitleMappingObjItem(tabArr[oriRepeatTabIndex].key);
            // 删除指定工程的音频映射对象
            workbenchStore.module.audioStore.setters.deleteAudioMappingObjItem(tabArr[oriRepeatTabIndex].key);
            // 删除指定工程标签页
            tabArr.splice(oriRepeatTabIndex, 1);
        },
        /**
         * changeActiveTab - 变更活动工程容器页
         *
         * @function
         * @param { string } tabKey - 目标工程容器页唯一性索引
         * */
        changeActiveTab: function (tabKey) {
            workbenchStore.state.projectObj.activeProjectKey = tabKey;
        },
        /**
         * loadTab - 导入一个新的工程容器页
         *
         * @function
         * @param { Array<Object> } newTabArr - 新工程标签对象数组
         * */
        loadTab: function (newTabArr) {
            const tabArr = workbenchStore.state.projectObj.projectArr;

            // 导入数据分分离出冲突标签
            const repeatTabArr = [];
            newTabArr.forEach((newTabItem) => {
                const repeatTab = tabArr.find((tabItem) => {
                    return tabItem.key === newTabItem.key;
                });

                if (repeatTab) {
                    repeatTabArr.push(newTabItem);
                } else {
                    workbenchStore.setters.projectConstruct(newTabItem);
                }
            });

            // 冲突标签处理
            if (repeatTabArr.length !== 0) {
                const repeatTabKeyArr = [];
                repeatTabArr.forEach((repeatTabItem) => {
                    repeatTabKeyArr.push(repeatTabItem.key);
                });

                ElMessageBox.confirm(
                    `导入工程数据与现有数据存在冲突, 请选择处理方式, 冲突工程有: ${ repeatTabKeyArr.join(', ') }`,
                    {
                        confirmButtonText: '覆盖现有数据',
                        cancelButtonText: '取消导入冲突数据',
                        type: 'warning',
                        showClose: false,
                        showCancelButton: true,
                        'close-on-press-escape': false,
                        'close-on-click-modal': false,
                    }
                ).then(() => {
                    repeatTabArr.forEach((repeatTabItem) => {
                        // 删除现有冲突工程对象
                        workbenchStore.setters.projectDestroy(repeatTabItem.key);

                        // 构建新工程对象
                        workbenchStore.setters.projectConstruct(repeatTabItem);
                    });
                }).catch(() => {});
            }
        },
        /**
         * changeTab - 切换工程容器页
         *
         * @function
         * @param { string } tabKey - 切换的工程页索引
         * */
        changeTab: function (tabKey) {
            // 获取标签对象
            const targetTab = workbenchStore.state.projectObj.projectArr.find((tabItem) => {
                return tabItem.key === tabKey;
            });

            // 切换 Peaks 链接音频
            if (targetTab !== void 0) {
                workbenchStore.module.audioStore.setters.changePeaksSource(targetTab.data.audioData.src);
            } else {
                workbenchStore.module.audioStore.setters.changePeaksSource('');
            }

            // 切换活动页标签索引
            workbenchStore.setters.changeActiveTab(tabKey);
        },
        /**
         * addTab - 添加工程容器页
         *
         * @function
         * */
        addTab: function () {
            const tabArr = workbenchStore.state.projectObj.projectArr;

            ElMessageBox.prompt(
                // 主体内容
                '请输入工程名',
                // 弹窗标题
                '新建工程',
                {
                    confirmButtonText: '确认',
                    cancelButtonText: '取消',
                    inputValidator: function (value) {
                        // 若 value 不存在, 则直接返回 false
                        if (value === null) {
                            return false;
                        }

                        // 不能为空, 不能有 . 符号, 不能重名
                        const notHavePeriod = !(/\./.test(value));
                        const notEmpty = value.trim() !== '';
                        const notRepeat = tabArr.find((item) => {
                            return item.key === value.trim();
                        }) === void 0;

                        return notHavePeriod && notEmpty && notRepeat;
                    },
                    inputErrorMessage: '工程名不能为空, 不能包含 . 符, 且不能重名!',
                }
            ).then(({ value }) => {
                // 构建工程虚拟对象
                const newTabItem = {
                    key: value.trim(),
                    data: {
                        audioData: {
                            src: '',
                            audioArrayBuffer: null,
                            decodeAudio: null,
                            currentTime: 0
                        },
                        // peaks 相关数据
                        peaksData: {},
                        // edit 相关数据
                        editData: {
                            // 字幕相关数据
                            subtitle: {
                                // 字幕对象组
                                contentArr: []
                            }
                        }
                    }
                }

                // 构建工程
                workbenchStore.setters.projectConstruct(newTabItem);
            }).catch(() => {});
        },
        /**
         * removeTab - 移除工程容器页
         *
         * @function
         * @param { String } targetName - 触发容器页名称
         * */
        removeTab: function (targetName) {
            ElMessageBox.confirm(
                '确认要删除当前工程嘛？',
                {
                    confirmButtonText: '确认',
                    cancelButtonText: '取消',
                    type: 'info',
                }
            ).then(() => {
                const tabArr = workbenchStore.state.projectObj.projectArr;

                // 删除指定工程
                workbenchStore.setters.projectDestroy(targetName);

                if (workbenchStore.state.projectObj.activeProjectKey === targetName && tabArr.length > 0) {
                    workbenchStore.setters.changeActiveTab(tabArr[0].key);
                }
            }).catch(() => {});
        },
        /**
         * changeEditView - 切换编辑区视图
         *
         * @function
         * @param { string } editViewKey - 切换的编辑区视图索引
         * */
        changeEditView: function (editViewKey) {
            workbenchStore.state.editBarOption.data.activeViewKey = editViewKey;
        },
        /**
         * changeShowView - 切换展示区视图
         *
         * @function
         * @param { string } showViewKey - 切换的展示区视图索引
         * */
        changeShowView: function (showViewKey) {
            workbenchStore.state.showBarOption.data.activeViewKey = showViewKey;
        },
        /**
         * changeToolItemObj - 修改工作台视图所属工具项样式配置
         *
         * @function
         * @param { string } viewBarKey - 工作台视图索引
         * @param { string } toolKey - 工具项索引
         * @param { Object } newOptionObj - 新配置项信息对象, 仅允许修改除 key 外的已有项属性, 不允许删除和添加新属性
         * */
        changeToolItemObj: function (viewBarKey, toolKey, newOptionObj) {
            // 取工作台目标视图对象
            let targetViewBarObj = null;
            if (workbenchStore.state.editBarOption.data.viewsObj[viewBarKey] !== void 0) {
                targetViewBarObj = workbenchStore.state.editBarOption.data.viewsObj[viewBarKey];
            } else if (workbenchStore.state.showBarOption.data.viewsObj[viewBarKey] !== void 0) {
                targetViewBarObj = workbenchStore.state.showBarOption.data.viewsObj[viewBarKey];
            } else {
                throw Error('未寻找指定工作台视图对象');
            }

            // 取目标工具项
            const targetToolItem = targetViewBarObj.toolArr.find((toolItem) => {
                return toolItem.key === toolKey;
            });
            if (targetToolItem === void 0) {
                // 未寻找指定工作台视图对象
                throw Error('未寻找指定工作台视图对象');
            }

            // 修改目标工具项
            Object.keys(targetToolItem).forEach((attributeKey) => {
                if (attributeKey !== 'key' && newOptionObj[attributeKey] !== void 0) {
                    targetToolItem[attributeKey] = newOptionObj[attributeKey];
                }
            });
        }
    },
    // 响应式对象模块
    module: {
        audioStore,
        subtitleStore,
        fileStore
    }
}

export default workbenchStore;

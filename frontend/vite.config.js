import { join } from 'path';
import vue from '@vitejs/plugin-vue'
import { loadEnv } from "vite";
// 引入 JSX 语法支持依赖
import vueJsx from "@vitejs/plugin-vue-jsx";
// 引入自定义 alias 配置
import aliasConfig from './alias.config.js';
// 引入自定义 rollup 详细配置
import rollupOption from './rollupOption.js';

/**
 * getEnvConfigItem - 获取 .env 配置项
 *
 * @function
 * @param { string } envKey - env 配置项索引
 * @return { string } 对应的 env 配置项设置
 * */
const getEnvConfigItem = function (envKey) {
    // loadEnv(process.argv[process.argv.length - 1], process.cwd()) 依据当前运行环境返回不同配置对象
    return loadEnv(process.argv[process.argv.length - 1], process.cwd())[envKey];
}

export default {
    root: join(__dirname, ''),              // 配置项目根目录(index.html 文件所在位置)
    // 开发或生产环境服务的公共基础路径, 本地环境用 './', 生产环境用 '/universal_audio_player/'
    base: getEnvConfigItem('VITE_BUILD_TYPE') === 'dev' ? './' : '/universal_audio_player/',
    publicDir: 'public',                    // 静态资源服务文件夹
    plugins: [
        vue(),
        vueJsx()
    ],
    resolve: {
        alias: aliasConfig.resolve.alias,   // 路径别名项配置
        extensions: [                       // 配置 import 默认支持的文件拓展后缀
            '.vue',
            '.js',
            '.jsx',
            '.ts',
            '.tsx',
            '.json'
        ]
    },
    // CSS 预处理
    css: {
        preprocessorOptions: {              // 配置全局预定义样式库
            scss: {
                // 若引入多个样式库使用 ; 分隔即可
                additionalData: '@import "@/assets/style/global.scss";',
                charset: false,             // 去除 vite 打包时的 CSS @charset Warning 提示
            }
        }
    },
    server: {
        port: getEnvConfigItem('VITE_BUILD_TYPE') === 'dev'
                ? getEnvConfigItem('VITE_APP_PORT_DEV')
                : getEnvConfigItem('VITE_APP_PORT_PROD'),
        https: false,                       // 是否开启 https
        open: false,                        // 是否在运行指令脚本后自动打开浏览器
        proxy: {}
    },
    build: {
        rollupOptions: rollupOption,
        outDir: join(__dirname, './dist'),  // 打包输出文件夹路径
        assetsDir: 'assets',                // 打包输出静态资源路径（相对于 outDir）
        emptyOutDir: true,                  // 是否允许打包时清空输出文件夹
        minify: 'terser',                   // 指定混入压缩器 - esbuild 或 terser
        assetsInlineLimit: 4096,            // 设置小于指定大小的资源以 base64 内联插入
        sourcemap: true,                    // 是否生成 sourcemap 映射文件
        manifest: true,                     // 是否生成模块资源引用表
        brotliSize: true,                   // 是否在命令行中展示打包输出报告
        terserOptions: {                    // 压缩器配置
            compress: {
                drop_console: true,         // 打包文件去除 console 语句
                drop_debugger: true         // 打包文件去除 debugger 语句
            }
        }
    }
}
